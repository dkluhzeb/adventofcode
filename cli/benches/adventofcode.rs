use criterion::BenchmarkId;
use criterion::Criterion;
use criterion::{criterion_group, criterion_main};
use day_1;
use day_10;
use day_11;
use day_2;
use day_3;
use day_4;
use day_5;
use day_6;
use day_7;
use day_8;
use day_9;
use std::path::Path;

fn run_day_1(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_1", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_1", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_1::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_1", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_1::part_2::run(p));
    });
}

fn run_day_2(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_2", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_2", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_2::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_2", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_2::part_2::run(p));
    });
}

fn run_day_3(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_3", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_3", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_3::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_3", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_3::part_2::run(p));
    });
}

fn run_day_4(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_4", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_4", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_4::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_4", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_4::part_2::run(p));
    });
}

fn run_day_5(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_5", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_5", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_5::part_1::run(p));
    });

    /* Disabled because of massive workload
    criterion.bench_with_input(BenchmarkId::new("day_5", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_5::part_2::run(p));
    });
    */
}

fn run_day_6(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_6", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_6", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_6::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_6", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_6::part_2::run(p));
    });
}

fn run_day_7(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_7", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_7", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_7::part_1::run(p));
    });

    /* Disabled because of massive workload
    criterion.bench_with_input(BenchmarkId::new("day_7", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_7::part_2::run(p));
    });
    */
}

fn run_day_8(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_8", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_8", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_8::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_8", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_8::part_2::run(p));
    });
}

fn run_day_9(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_9", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_9", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_9::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_9", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_9::part_2::run(p));
    });
}

fn run_day_10(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_10", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_10", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_10::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_10", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_10::part_2::run(p));
    });
}

fn run_day_11(criterion: &mut Criterion) {
    let fixture = format!("{}/benches/fixtures/day_11", env!("CARGO_MANIFEST_DIR"));
    let file_path = Path::new(&fixture);

    criterion.bench_with_input(BenchmarkId::new("day_11", "part_1"), &file_path, |b, &p| {
        b.iter(|| day_11::part_1::run(p));
    });

    criterion.bench_with_input(BenchmarkId::new("day_11", "part_2"), &file_path, |b, &p| {
        b.iter(|| day_11::part_2::run(p));
    });
}

criterion_group!(
    benches, run_day_1, run_day_2, run_day_3, run_day_4, run_day_5, run_day_6, run_day_7,
    run_day_8, run_day_9, run_day_10, run_day_11
);
criterion_main!(benches);
