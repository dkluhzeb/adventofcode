use clap::{Args, Parser, Subcommand, ValueEnum};
use std::{error::Error, fmt, path::PathBuf};

use day_1;
use day_10;
use day_11;
use day_2;
use day_3;
use day_4;
use day_5;
use day_6;
use day_7;
use day_8;
use day_9;

#[derive(Parser)]
#[command(author, version, about, long_about = None, arg_required_else_help(true))]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Debug, Args)]
struct DayArgs {
    /// path to the input file
    #[arg(short, long)]
    input_path: PathBuf,

    /// part of the puzzle
    #[arg(short, long, value_enum, default_value = "part1")]
    part: Part,
}

#[derive(Clone, Debug, ValueEnum)]
enum Part {
    /// Part 1 of a Puzzle.
    Part1,
    /// Part 2 of a Puzzle.
    Part2,
}

#[derive(Subcommand)]
enum Commands {
    /// Day 1: Trebuchet?!
    #[command(long_about=include_str!("../../day_1/Readme.md"))]
    Day1(DayArgs),
    /// Day 2: Cube Conundrum
    #[command(long_about=include_str!("../../day_2/Readme.md"))]
    Day2(DayArgs),
    /// Day 3: Gear Ratios
    #[command(long_about=include_str!("../../day_3/Readme.md"))]
    Day3(DayArgs),
    /// Day 4: Scratchcards
    #[command(long_about=include_str!("../../day_4/Readme.md"))]
    Day4(DayArgs),
    /// Day 5: If You Give A Seed A Fertilizer
    #[command(long_about=include_str!("../../day_5/Readme.md"))]
    Day5(DayArgs),
    /// Day 6: Wait For It
    #[command(long_about=include_str!("../../day_6/Readme.md"))]
    Day6(DayArgs),
    /// Day 7: Camel Cards
    #[command(long_about=include_str!("../../day_7/Readme.md"))]
    Day7(DayArgs),
    /// Day 8: Haunted Wasteland
    #[command(long_about=include_str!("../../day_8/Readme.md"))]
    Day8(DayArgs),
    /// Day 9: Mirage Maintenance
    #[command(long_about=include_str!("../../day_9/Readme.md"))]
    Day9(DayArgs),
    /// Day 10: Pipe Maze
    #[command(long_about=include_str!("../../day_10/Readme.md"))]
    Day10(DayArgs),
    /// Day 11: Cosmic Expansion
    #[command(long_about=include_str!("../../day_11/Readme.md"))]
    Day11(DayArgs),
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Some(Commands::Day1(args)) => match args.part {
            Part::Part1 => print_result(day_1::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_1::part_2::run(&args.input_path)),
        },
        Some(Commands::Day2(args)) => match args.part {
            Part::Part1 => print_result(day_2::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_2::part_2::run(&args.input_path)),
        },
        Some(Commands::Day3(args)) => match args.part {
            Part::Part1 => print_result(day_3::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_3::part_2::run(&args.input_path)),
        },
        Some(Commands::Day4(args)) => match args.part {
            Part::Part1 => print_result(day_4::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_4::part_2::run(&args.input_path)),
        },
        Some(Commands::Day5(args)) => match args.part {
            Part::Part1 => print_result(day_5::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_5::part_2::run(&args.input_path)),
        },
        Some(Commands::Day6(args)) => match args.part {
            Part::Part1 => print_result(day_6::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_6::part_2::run(&args.input_path)),
        },
        Some(Commands::Day7(args)) => match args.part {
            Part::Part1 => print_result(day_7::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_7::part_2::run(&args.input_path)),
        },
        Some(Commands::Day8(args)) => match args.part {
            Part::Part1 => print_result(day_8::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_8::part_2::run(&args.input_path)),
        },
        Some(Commands::Day9(args)) => match args.part {
            Part::Part1 => print_result(day_9::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_9::part_2::run(&args.input_path)),
        },
        Some(Commands::Day10(args)) => match args.part {
            Part::Part1 => print_result(day_10::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_10::part_2::run(&args.input_path)),
        },
        Some(Commands::Day11(args)) => match args.part {
            Part::Part1 => print_result(day_11::part_1::run(&args.input_path)),
            Part::Part2 => print_result(day_11::part_2::run(&args.input_path)),
        },
        None => {
            println!("Please provide a command.");
        }
    }
}

fn print_result<T>(result: Result<T, Box<dyn Error>>) -> ()
where
    T: fmt::Debug,
{
    match result {
        Ok(res) => println!("{:#?}", res),
        Err(e) => eprintln!("{:#?}", e),
    }
}
