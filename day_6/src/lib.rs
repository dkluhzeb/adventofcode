use std::{fs::read_to_string, iter::zip, num::ParseIntError, path::Path};

pub mod part_1;
pub mod part_2;

/// The RaceHistory holds all previous races
#[derive(Debug, PartialEq)]
pub struct RaceHistory {
    /// Previous races
    pub races: Vec<Race>,
}

/// A race
#[derive(Debug, PartialEq)]
pub struct Race {
    /// Time (duration) of this race
    pub time: usize,
    /// The record distance of this race
    pub distance: usize,
}

impl RaceHistory {
    /// Creates a new race history
    pub fn new() -> Self {
        RaceHistory { races: Vec::new() }
    }

    /// Appends a vector of races to the race history
    pub fn append_races(self: &mut Self, races: &mut Vec<Race>) {
        self.races.append(races)
    }

    /// Multiplies all record breaking strategies count per race of all previous races
    /// (Solution for part 1)
    pub fn get_multiplied_rbs(self: &Self) -> usize {
        self.races.iter().fold(1, |acc, race| {
            acc * race.count_record_breaking_strategies() as usize
        })
    }
}

impl Race {
    /// Creates a new race
    pub fn new(time: usize, distance: usize) -> Self {
        Race { time, distance }
    }

    /// Count all record breaking strategies for this race
    pub fn count_record_breaking_strategies(self: &Self) -> usize {
        let mut to_slow_count: usize = 0;

        for press_time in 0..self.time {
            let distance = (self.time - press_time) * press_time;

            if distance <= self.distance {
                to_slow_count += 1;
            } else {
                break;
            }
        }

        for press_time in (0..self.time).rev() {
            let distance = (self.time - press_time) * press_time;

            if distance <= self.distance {
                to_slow_count += 1;
            } else {
                break;
            }
        }

        self.time - to_slow_count
    }
}

impl TryFrom<&Path> for RaceHistory {
    type Error = &'static str;

    /// Creates a race history from a given file path
    /// For required format have a look at the fixture
    fn try_from(file_path: &Path) -> Result<Self, Self::Error> {
        let file_contents = read_to_string(file_path);

        if file_contents.is_err() {
            return Err("Unable to read file.");
        }

        let mut race_history = RaceHistory::new();
        let mut even: Option<&str> = None;

        for line in file_contents.unwrap().lines() {
            let trimmed_line = line.trim();

            if trimmed_line == "" {
                continue;
            }

            if trimmed_line.starts_with("Time:") {
                even = Some(&trimmed_line[5..]);
                continue;
            }

            if trimmed_line.starts_with("Distance:") {
                if even.is_none() {
                    return Err(
                        "Unable to parse file content - No times for distances line found.",
                    );
                }

                if let (Some(times), Some(distances)) = (
                    parse_numbers(even.unwrap()),
                    parse_numbers(&trimmed_line[9..]),
                ) {
                    race_history.append_races(
                        &mut zip(times, distances)
                            .map(|race| Race::new(race.0, race.1))
                            .collect(),
                    );
                } else {
                    return Err(
                        "Unable to parse file content - Numbers parsing returns empty for line.",
                    );
                }
            }
        }

        Ok(race_history)
    }
}

impl TryFrom<&RaceHistory> for Race {
    type Error = &'static str;

    /// Creates a race from a race history
    /// The data of the previous races get combined
    /// This is needed for the solution of part2
    /// Have a look at the puzzle question for more information
    fn try_from(race_history: &RaceHistory) -> Result<Self, Self::Error> {
        if race_history.races.len() == 0 {
            return Err("No races to combine.");
        }

        let combined: (String, String) =
            race_history
                .races
                .iter()
                .fold((String::new(), String::new()), |mut acc, race| {
                    acc.0.push_str(race.time.to_string().as_str());
                    acc.1.push_str(race.distance.to_string().as_str());

                    acc
                });

        match (combined.0.parse::<usize>(), combined.1.parse::<usize>()) {
            (Ok(time), Ok(distance)) => Ok(Race::new(time, distance)),
            _ => Err("Failed to combine races."),
        }
    }
}

/// Parses the numbers from a string slice
/// Required format: 41 48 83 86 17
fn parse_numbers(str: &str) -> Option<Vec<usize>> {
    let res: Result<Vec<usize>, ParseIntError> = str
        .trim()
        .split(" ")
        .filter(|char| char != &"") // remove empty kinds (multiple whitespace delimiters)
        .map(|number| number.trim().parse::<usize>())
        .collect();

    match res {
        Ok(numbers) => Some(numbers),
        Err(..) => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_race_history_from_path() {
        assert_eq!(
            RaceHistory::try_from(Path::new(
                format!("{}/fixtures/part_1", env!("CARGO_MANIFEST_DIR")).as_str()
            )),
            Ok(get_race_history())
        );
    }

    #[test]
    fn test_get_multiplied_rbs() {
        assert_eq!(get_race_history().get_multiplied_rbs(), 288);
    }

    #[test]
    fn test_race_from_race_history() {
        assert_eq!(
            Race::try_from(&get_race_history()),
            Ok(Race {
                time: 71530,
                distance: 940200
            })
        );
    }

    #[test]
    fn test_count_record_breaking_strategies() {
        assert_eq!(
            Race {
                time: 7,
                distance: 9
            }
            .count_record_breaking_strategies(),
            4
        );

        assert_eq!(
            Race {
                time: 15,
                distance: 40
            }
            .count_record_breaking_strategies(),
            8
        );

        assert_eq!(
            Race {
                time: 30,
                distance: 200
            }
            .count_record_breaking_strategies(),
            9
        );

        assert_eq!(
            Race {
                time: 71530,
                distance: 940200
            }
            .count_record_breaking_strategies(),
            71503
        );
    }

    fn get_race_history() -> RaceHistory {
        RaceHistory {
            races: Vec::from([
                Race {
                    time: 7,
                    distance: 9,
                },
                Race {
                    time: 15,
                    distance: 40,
                },
                Race {
                    time: 30,
                    distance: 200,
                },
            ]),
        }
    }
}
