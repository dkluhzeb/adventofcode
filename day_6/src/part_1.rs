use crate::RaceHistory;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/6
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(RaceHistory::try_from(file_path)?.get_multiplied_rbs())
}
