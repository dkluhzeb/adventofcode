use crate::{Race, RaceHistory};
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/6#part2
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(Race::try_from(&RaceHistory::try_from(file_path)?)?.count_record_breaking_strategies())
}
