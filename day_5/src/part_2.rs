use crate::Almanac;
use std::{error::Error, fs::read_to_string, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/5#part2
pub fn run(file_path: &Path) -> Result<Option<usize>, Box<dyn Error>> {
    let mut almanac = Almanac::try_from(read_to_string(file_path)?.as_str())?;

    almanac.create_seed_ranges();

    Ok(almanac.find_lowest_location())
}
