use std::{collections::HashMap, num::ParseIntError};

pub mod part_1;
pub mod part_2;

#[derive(Clone, Debug, PartialEq)]
struct AlmanacMap {
    destination_range_start: usize,
    source_range_start: usize,
    range_length: usize,
}

#[derive(Debug, PartialEq)]
pub struct Almanac {
    seed_ids: Vec<usize>,
    seed_to_soil: Vec<AlmanacMap>,
    soil_to_fertilizer: Vec<AlmanacMap>,
    fertilizer_to_water: Vec<AlmanacMap>,
    water_to_light: Vec<AlmanacMap>,
    light_to_temperature: Vec<AlmanacMap>,
    temperature_to_humidity: Vec<AlmanacMap>,
    humidity_to_location: Vec<AlmanacMap>,
}

#[derive(Debug, PartialEq)]
pub struct Instruction {
    seed: usize,
    soil: usize,
    fertilizer: usize,
    water: usize,
    light: usize,
    temperature: usize,
    humidity: usize,
    location: usize,
}

impl TryFrom<&str> for AlmanacMap {
    type Error = &'static str;

    fn try_from(str: &str) -> Result<Self, Self::Error> {
        match parse_numbers(str) {
            Some(parts) => {
                if parts.len() != 3 {
                    return Err("Almanac map parsing failed - Invalid numbers count in string.");
                }

                let mut iter = parts.into_iter();

                Ok(AlmanacMap {
                    destination_range_start: iter.next().unwrap(),
                    source_range_start: iter.next().unwrap(),
                    range_length: iter.next().unwrap(),
                })
            }
            None => Err("Almanac map parsing failed - No numbers in string."),
        }
    }
}

impl TryFrom<&str> for Almanac {
    type Error = &'static str;

    fn try_from(str: &str) -> Result<Self, Self::Error> {
        let mut seed_ids: Option<Vec<usize>> = None;
        let mut lists: HashMap<&str, Vec<AlmanacMap>> = HashMap::new();
        let mut current_list_name: Option<&str> = None;

        for line in str.lines() {
            let trimmed_line = line.trim();

            if trimmed_line.len() == 0 {
                continue;
            }

            if trimmed_line.starts_with("seeds:") {
                seed_ids = parse_numbers(&trimmed_line[6..]);

                if seed_ids.is_none() {
                    return Err("Parsing almanac failed - Invalid seed ids.");
                }

                continue;
            }

            if trimmed_line.ends_with("map:") {
                if let Some(map_name) = trimmed_line.split(" ").next() {
                    current_list_name = Some(map_name);
                } else {
                    return Err("Parsing almanac failed - Invalid map name.");
                }

                continue;
            }

            if let (Some(list_name), new_map) =
                (current_list_name, AlmanacMap::try_from(trimmed_line)?)
            {
                if let Some(list) = lists.get_mut(&list_name) {
                    list.push(new_map);
                } else {
                    lists.insert(&list_name, Vec::from([new_map]));
                }
            } else {
                return Err("Parsing almanac failed - No list name.");
            }
        }

        match (
            seed_ids,
            lists.get("seed-to-soil"),
            lists.get("soil-to-fertilizer"),
            lists.get("fertilizer-to-water"),
            lists.get("water-to-light"),
            lists.get("light-to-temperature"),
            lists.get("temperature-to-humidity"),
            lists.get("humidity-to-location"),
        ) {
            (
                Some(seed_ids),
                Some(seed_to_soil),
                Some(soil_to_fertilizer),
                Some(fertilizer_to_water),
                Some(water_to_light),
                Some(light_to_temperature),
                Some(temperature_to_humidity),
                Some(humidity_to_location),
            ) => Ok(Almanac {
                seed_ids,
                seed_to_soil: seed_to_soil.to_vec(),
                soil_to_fertilizer: soil_to_fertilizer.to_vec(),
                fertilizer_to_water: fertilizer_to_water.to_vec(),
                water_to_light: water_to_light.to_vec(),
                light_to_temperature: light_to_temperature.to_vec(),
                temperature_to_humidity: temperature_to_humidity.to_vec(),
                humidity_to_location: humidity_to_location.to_vec(),
            }),
            _ => Err("Parsing almanac failed - Missing"),
        }
    }
}

impl Almanac {
    pub fn create_seed_ranges(self: &mut Self) {
        let mut new_seed_ids: Vec<usize> = Vec::new();
        let mut even: Option<usize> = None;

        for number in self.seed_ids.iter() {
            if let Some(range_start) = even {
                new_seed_ids.extend((range_start..(range_start + number)).collect::<Vec<usize>>());
                even = None;
            } else {
                even = Some(*number);
            }
        }

        self.seed_ids = new_seed_ids;
    }

    pub fn get_instructions(self: Self) -> Vec<Instruction> {
        self.seed_ids
            .iter()
            .map(|seed| {
                let soil = self.find_soil_by_seed(*seed);
                let fertilizer = self.find_fertilizer_by_soil(soil);
                let water = self.find_water_by_fertilizer(fertilizer);
                let light = self.find_light_by_water(water);
                let temperature = self.find_temperature_by_light(light);
                let humidity = self.find_humidity_by_temperature(temperature);
                let location = self.find_location_by_humidity(humidity);

                Instruction {
                    seed: *seed,
                    soil,
                    fertilizer,
                    water,
                    light,
                    temperature,
                    humidity,
                    location,
                }
            })
            .collect::<Vec<Instruction>>()
    }

    fn find_soil_by_seed(self: &Self, seed: usize) -> usize {
        for soil_map in self.seed_to_soil.iter() {
            if let Some(soil) = soil_map.find(seed) {
                return soil;
            }
        }

        seed
    }

    fn find_fertilizer_by_soil(self: &Self, soil: usize) -> usize {
        for fertilizer_map in self.soil_to_fertilizer.iter() {
            if let Some(fertilizer) = fertilizer_map.find(soil) {
                return fertilizer;
            }
        }

        soil
    }

    fn find_water_by_fertilizer(self: &Self, fertilizer: usize) -> usize {
        for water_map in self.fertilizer_to_water.iter() {
            if let Some(water) = water_map.find(fertilizer) {
                return water;
            }
        }

        fertilizer
    }

    fn find_light_by_water(self: &Self, water: usize) -> usize {
        for light_map in self.water_to_light.iter() {
            if let Some(light) = light_map.find(water) {
                return light;
            }
        }

        water
    }

    fn find_temperature_by_light(self: &Self, light: usize) -> usize {
        for temperature_map in self.light_to_temperature.iter() {
            if let Some(temperature) = temperature_map.find(light) {
                return temperature;
            }
        }

        light
    }

    fn find_humidity_by_temperature(self: &Self, temperature: usize) -> usize {
        for humidity_map in self.temperature_to_humidity.iter() {
            if let Some(humidity) = humidity_map.find(temperature) {
                return humidity;
            }
        }

        temperature
    }

    fn find_location_by_humidity(self: &Self, humidity: usize) -> usize {
        for location_map in self.humidity_to_location.iter() {
            if let Some(location) = location_map.find(humidity) {
                return location;
            }
        }

        humidity
    }

    pub fn find_lowest_location(self: &Self) -> Option<usize> {
        self.seed_ids.iter().fold(None, |acc, seed| {
            let location = self.find_location_by_humidity(self.find_humidity_by_temperature(
                self.find_temperature_by_light(self.find_light_by_water(
                    self.find_water_by_fertilizer(
                        self.find_fertilizer_by_soil(self.find_soil_by_seed(*seed)),
                    ),
                )),
            ));

            if acc.is_none() || location < acc.unwrap() {
                Some(location)
            } else {
                acc
            }
        })
    }
}

impl AlmanacMap {
    fn find(self: &Self, source: usize) -> Option<usize> {
        let source_range_end = self.source_range_start + self.range_length;

        if self.source_range_start > source || source >= source_range_end {
            return None;
        }

        let candidate = self.destination_range_start + source - self.source_range_start;

        if self.destination_range_start > candidate
            || candidate >= self.destination_range_start + self.range_length
        {
            return None;
        }

        Some(candidate)
    }
}

/// Parses the numbers from a string slice
/// Required format: 41 48 83 86 17
fn parse_numbers(str: &str) -> Option<Vec<usize>> {
    let res: Result<Vec<usize>, ParseIntError> = str
        .trim()
        .split(" ")
        .filter(|char| char != &"") // remove empty kinds (multiple whitespace delimiters)
        .map(|number| number.trim().parse::<usize>())
        .collect();

    match res {
        Ok(numbers) => Some(numbers),
        Err(..) => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_almanac_from_str() {
        let fixture = include_str!("../fixtures/part_1");

        assert_eq!(Almanac::try_from(fixture), Ok(get_almanac()));
    }

    #[test]
    fn test_find_lowest_location() {
        assert_eq!(get_almanac().find_lowest_location(), Some(35));
    }

    #[test]
    fn test_find_soil_by_seed() {
        assert_eq!(get_almanac().find_soil_by_seed(79), 81);
        assert_eq!(get_almanac().find_soil_by_seed(14), 14);
        assert_eq!(get_almanac().find_soil_by_seed(55), 57);
        assert_eq!(get_almanac().find_soil_by_seed(13), 13);
    }

    #[test]
    fn test_find_fertilizer_by_soil() {
        assert_eq!(get_almanac().find_fertilizer_by_soil(81), 81);
        assert_eq!(get_almanac().find_fertilizer_by_soil(14), 53);
        assert_eq!(get_almanac().find_fertilizer_by_soil(57), 57);
        assert_eq!(get_almanac().find_fertilizer_by_soil(13), 52);
    }

    #[test]
    fn test_find_water_by_fertilizer() {
        assert_eq!(get_almanac().find_water_by_fertilizer(81), 81);
        assert_eq!(get_almanac().find_water_by_fertilizer(53), 49);
        assert_eq!(get_almanac().find_water_by_fertilizer(57), 53);
        assert_eq!(get_almanac().find_water_by_fertilizer(52), 41);
    }

    #[test]
    fn test_find_light_by_water() {
        assert_eq!(get_almanac().find_light_by_water(81), 74);
        assert_eq!(get_almanac().find_light_by_water(49), 42);
        assert_eq!(get_almanac().find_light_by_water(53), 46);
        assert_eq!(get_almanac().find_light_by_water(41), 34);
    }

    #[test]
    fn test_find_temperature_by_light() {
        assert_eq!(get_almanac().find_temperature_by_light(74), 78);
        assert_eq!(get_almanac().find_temperature_by_light(42), 42);
        assert_eq!(get_almanac().find_temperature_by_light(46), 82);
        assert_eq!(get_almanac().find_temperature_by_light(34), 34);
    }

    #[test]
    fn test_find_humidity_by_temperature() {
        assert_eq!(get_almanac().find_humidity_by_temperature(78), 78);
        assert_eq!(get_almanac().find_humidity_by_temperature(42), 43);
        assert_eq!(get_almanac().find_humidity_by_temperature(82), 82);
        assert_eq!(get_almanac().find_humidity_by_temperature(34), 35);
    }

    #[test]
    fn test_find_location_by_humidity() {
        assert_eq!(get_almanac().find_location_by_humidity(78), 82);
        assert_eq!(get_almanac().find_location_by_humidity(43), 43);
        assert_eq!(get_almanac().find_location_by_humidity(82), 86);
        assert_eq!(get_almanac().find_location_by_humidity(35), 35);
    }

    #[test]
    fn test_get_instructions() {
        assert_eq!(
            get_almanac().get_instructions(),
            Vec::from([
                Instruction {
                    seed: 79,
                    soil: 81,
                    fertilizer: 81,
                    water: 81,
                    light: 74,
                    temperature: 78,
                    humidity: 78,
                    location: 82,
                },
                Instruction {
                    seed: 14,
                    soil: 14,
                    fertilizer: 53,
                    water: 49,
                    light: 42,
                    temperature: 42,
                    humidity: 43,
                    location: 43,
                },
                Instruction {
                    seed: 55,
                    soil: 57,
                    fertilizer: 57,
                    water: 53,
                    light: 46,
                    temperature: 82,
                    humidity: 82,
                    location: 86,
                },
                Instruction {
                    seed: 13,
                    soil: 13,
                    fertilizer: 52,
                    water: 41,
                    light: 34,
                    temperature: 34,
                    humidity: 35,
                    location: 35,
                }
            ])
        );
    }

    fn get_almanac() -> Almanac {
        Almanac {
            seed_ids: Vec::from([79, 14, 55, 13]),
            seed_to_soil: Vec::from([
                AlmanacMap {
                    destination_range_start: 50,
                    source_range_start: 98,
                    range_length: 2,
                },
                AlmanacMap {
                    destination_range_start: 52,
                    source_range_start: 50,
                    range_length: 48,
                },
            ]),
            soil_to_fertilizer: Vec::from([
                AlmanacMap {
                    destination_range_start: 0,
                    source_range_start: 15,
                    range_length: 37,
                },
                AlmanacMap {
                    destination_range_start: 37,
                    source_range_start: 52,
                    range_length: 2,
                },
                AlmanacMap {
                    destination_range_start: 39,
                    source_range_start: 0,
                    range_length: 15,
                },
            ]),
            fertilizer_to_water: Vec::from([
                AlmanacMap {
                    destination_range_start: 49,
                    source_range_start: 53,
                    range_length: 8,
                },
                AlmanacMap {
                    destination_range_start: 0,
                    source_range_start: 11,
                    range_length: 42,
                },
                AlmanacMap {
                    destination_range_start: 42,
                    source_range_start: 0,
                    range_length: 7,
                },
                AlmanacMap {
                    destination_range_start: 57,
                    source_range_start: 7,
                    range_length: 4,
                },
            ]),
            water_to_light: Vec::from([
                AlmanacMap {
                    destination_range_start: 88,
                    source_range_start: 18,
                    range_length: 7,
                },
                AlmanacMap {
                    destination_range_start: 18,
                    source_range_start: 25,
                    range_length: 70,
                },
            ]),
            light_to_temperature: Vec::from([
                AlmanacMap {
                    destination_range_start: 45,
                    source_range_start: 77,
                    range_length: 23,
                },
                AlmanacMap {
                    destination_range_start: 81,
                    source_range_start: 45,
                    range_length: 19,
                },
                AlmanacMap {
                    destination_range_start: 68,
                    source_range_start: 64,
                    range_length: 13,
                },
            ]),
            temperature_to_humidity: Vec::from([
                AlmanacMap {
                    destination_range_start: 0,
                    source_range_start: 69,
                    range_length: 1,
                },
                AlmanacMap {
                    destination_range_start: 1,
                    source_range_start: 0,
                    range_length: 69,
                },
            ]),
            humidity_to_location: Vec::from([
                AlmanacMap {
                    destination_range_start: 60,
                    source_range_start: 56,
                    range_length: 37,
                },
                AlmanacMap {
                    destination_range_start: 56,
                    source_range_start: 93,
                    range_length: 4,
                },
            ]),
        }
    }
}
