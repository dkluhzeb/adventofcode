use crate::Almanac;
use std::{error::Error, fs::read_to_string, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/5
pub fn run(file_path: &Path) -> Result<Option<usize>, Box<dyn Error>> {
    Ok(Almanac::try_from(read_to_string(file_path)?.as_str())?.find_lowest_location())
}
