use crate::OasisReport;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/9#part2
pub fn run(file_path: &Path) -> Result<isize, Box<dyn Error>> {
    Ok(OasisReport::try_from(file_path)?.get_previous_sum())
}
