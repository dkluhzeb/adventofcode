use std::{error::Error, fs::read_to_string, num::ParseIntError, path::Path};

pub mod part_1;
pub mod part_2;

/// This struct reprensents an oasis report.
#[derive(Debug, PartialEq)]
pub struct OasisReport {
    // A collection of histories.
    histories: Vec<History>,
}

/// This struct reprensents a history.
#[derive(Debug, PartialEq)]
pub struct History {
    /// Available history entries.
    entries: Vec<Vec<isize>>,
}

impl OasisReport {
    /// Sum up all next values of all known histories. (Part 1)
    pub fn get_next_sum(self: Self) -> isize {
        self.histories
            .into_iter()
            .fold(0, |acc, history| acc + history.get_next_value())
    }

    /// Sum up all previous values of all known histories. (Part 2)
    pub fn get_previous_sum(self: Self) -> isize {
        self.histories
            .into_iter()
            .fold(0, |acc, history| acc + history.get_previous_value())
    }
}

impl TryFrom<&Path> for OasisReport {
    type Error = Box<dyn Error>;

    /// Creates an oasis report from a given file path.
    /// For file format have a look at the fixtures.
    fn try_from(file_path: &Path) -> Result<Self, Box<dyn Error>> {
        let file_contents = read_to_string(file_path)?;
        let mut histories: Vec<History> = Vec::new();

        for line in file_contents.lines() {
            let trimmed_line = line.trim();
            let split = trimmed_line.split(" ");

            let initial_numbers = split
                .into_iter()
                .map(|number| number.trim().parse::<isize>())
                .collect::<Result<Vec<isize>, ParseIntError>>()?;

            histories.push(History::new(initial_numbers));
        }

        Ok(OasisReport { histories })
    }
}

impl History {
    /// Creates a new history.
    pub fn new(current_entry: Vec<isize>) -> Self {
        History {
            entries: History::calculate_entries(current_entry),
        }
    }

    /// Calculates the history steps backt until zero.
    pub fn calculate_entries(current_entry: Vec<isize>) -> Vec<Vec<isize>> {
        let mut entries: Vec<Vec<isize>> = Vec::from([current_entry.clone()]);
        let mut last_number: Option<isize> = None;
        let mut new_entry: Vec<isize> = Vec::new();

        loop {
            for number in entries.last().unwrap().into_iter() {
                if last_number.is_some() {
                    new_entry.push(number - last_number.unwrap());
                }

                last_number = Some(*number);
            }

            entries.push(new_entry.clone());

            last_number = None;

            if new_entry.iter().all(|entry| entry == &0) {
                break;
            }

            new_entry = Vec::new();
        }

        entries
    }

    /// Get the next history value.
    pub fn get_next_value(self: Self) -> isize {
        self.entries
            .into_iter()
            .rev()
            .fold(0, |acc, entry| acc + entry.last().expect("Empty entry."))
    }

    /// Get the previous history value.
    pub fn get_previous_value(self: Self) -> isize {
        self.entries
            .into_iter()
            .rev()
            .fold(0, |acc, entry| entry.first().expect("Empty entry.") - acc)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_oasis_report_from_path() {
        assert_eq!(
            OasisReport::try_from(Path::new(
                format!("{}/fixtures/example", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_oasis_report()
        );
    }

    #[test]
    fn test_get_next_sum() {
        assert_eq!(get_oasis_report().get_next_sum(), 114);
    }

    #[test]
    fn test_get_previous_sum() {
        assert_eq!(get_oasis_report().get_previous_sum(), 2);
    }

    #[test]
    fn test_history_get_next_value() {
        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([0, 3, 6, 9, 12, 15]),
                    Vec::from([3, 3, 3, 3, 3]),
                    Vec::from([0, 0, 0, 0]),
                ]),
            }
            .get_next_value(),
            18
        );

        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([1, 3, 6, 10, 15, 21]),
                    Vec::from([2, 3, 4, 5, 6]),
                    Vec::from([1, 1, 1, 1]),
                    Vec::from([0, 0, 0]),
                ]),
            }
            .get_next_value(),
            28
        );

        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([10, 13, 16, 21, 30, 45]),
                    Vec::from([3, 3, 5, 9, 15]),
                    Vec::from([0, 2, 4, 6]),
                    Vec::from([2, 2, 2]),
                    Vec::from([0, 0]),
                ]),
            }
            .get_next_value(),
            68
        );
    }

    #[test]
    fn test_history_get_previous_value() {
        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([0, 3, 6, 9, 12, 15]),
                    Vec::from([3, 3, 3, 3, 3]),
                    Vec::from([0, 0, 0, 0]),
                ]),
            }
            .get_previous_value(),
            -3
        );

        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([1, 3, 6, 10, 15, 21]),
                    Vec::from([2, 3, 4, 5, 6]),
                    Vec::from([1, 1, 1, 1]),
                    Vec::from([0, 0, 0]),
                ]),
            }
            .get_previous_value(),
            0
        );

        assert_eq!(
            History {
                entries: Vec::from([
                    Vec::from([10, 13, 16, 21, 30, 45]),
                    Vec::from([3, 3, 5, 9, 15]),
                    Vec::from([0, 2, 4, 6]),
                    Vec::from([2, 2, 2]),
                    Vec::from([0, 0]),
                ]),
            }
            .get_previous_value(),
            5
        );
    }

    #[test]
    fn test_history_calculate_entries() {
        assert_eq!(
            History::calculate_entries(Vec::from([0, 3, 6, 9, 12, 15])),
            Vec::from([
                Vec::from([0, 3, 6, 9, 12, 15]),
                Vec::from([3, 3, 3, 3, 3]),
                Vec::from([0, 0, 0, 0]),
            ])
        );

        assert_eq!(
            History::calculate_entries(Vec::from([1, 3, 6, 10, 15, 21])),
            Vec::from([
                Vec::from([1, 3, 6, 10, 15, 21]),
                Vec::from([2, 3, 4, 5, 6]),
                Vec::from([1, 1, 1, 1]),
                Vec::from([0, 0, 0]),
            ])
        );

        assert_eq!(
            History::calculate_entries(Vec::from([10, 13, 16, 21, 30, 45])),
            Vec::from([
                Vec::from([10, 13, 16, 21, 30, 45]),
                Vec::from([3, 3, 5, 9, 15]),
                Vec::from([0, 2, 4, 6]),
                Vec::from([2, 2, 2]),
                Vec::from([0, 0]),
            ])
        );
    }

    fn get_oasis_report() -> OasisReport {
        OasisReport {
            histories: Vec::from([
                History {
                    entries: Vec::from([
                        Vec::from([0, 3, 6, 9, 12, 15]),
                        Vec::from([3, 3, 3, 3, 3]),
                        Vec::from([0, 0, 0, 0]),
                    ]),
                },
                History {
                    entries: Vec::from([
                        Vec::from([1, 3, 6, 10, 15, 21]),
                        Vec::from([2, 3, 4, 5, 6]),
                        Vec::from([1, 1, 1, 1]),
                        Vec::from([0, 0, 0]),
                    ]),
                },
                History {
                    entries: Vec::from([
                        Vec::from([10, 13, 16, 21, 30, 45]),
                        Vec::from([3, 3, 5, 9, 15]),
                        Vec::from([0, 2, 4, 6]),
                        Vec::from([2, 2, 2]),
                        Vec::from([0, 0]),
                    ]),
                },
            ]),
        }
    }
}
