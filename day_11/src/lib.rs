use std::{collections::HashMap, error::Error, fs::read_to_string, path::Path};

pub mod part_1;
pub mod part_2;

#[derive(Debug, PartialEq)]
pub struct TelescopeImage {
    raster: Vec<Vec<char>>,
    galaxies: Option<Vec<(usize, usize)>>,
}

impl TelescopeImage {
    fn calculate_distances(self: &mut Self, expansion: usize) -> usize {
        self.mark_empty_space();
        self.find_galaxies(expansion);

        let mut distances: HashMap<[usize; 2], usize> = HashMap::new();

        for (first_index, first_galaxy) in self.galaxies.clone().unwrap().iter().enumerate() {
            for (second_index, second_galaxy) in self.galaxies.clone().unwrap().iter().enumerate() {
                let mut id: [usize; 2] = [first_index, second_index];

                id.sort();

                let distance = first_galaxy.0.abs_diff(second_galaxy.0)
                    + first_galaxy.1.abs_diff(second_galaxy.1);

                distances.insert(id, distance);
            }
        }

        distances.into_values().sum()
    }

    fn find_galaxies(self: &mut Self, expansion: usize) {
        let mut galaxies: Vec<(usize, usize)> = Vec::new();
        let mut y_expansion = 0;
        let mut x_expansion = 0;

        for (y, row) in self.raster.iter().enumerate() {
            if row.iter().all(|char| char == &'x') {
                y_expansion += expansion - 1;
            }

            for (x, char) in row.iter().enumerate() {
                if char == &'x' {
                    x_expansion += expansion - 1;
                }

                if char == &'#' {
                    galaxies.push((x + x_expansion, y + y_expansion));
                }
            }

            x_expansion = 0;
        }

        self.galaxies = Some(galaxies);
    }

    fn mark_empty_space(self: &mut Self) {
        self.mark_empty_cols();
        self.mark_empty_rows();
    }

    fn mark_empty_cols(self: &mut Self) {
        let empty_colums =
            self.raster.clone()[0]
                .iter()
                .enumerate()
                .fold(Vec::new(), |mut acc, (x, char)| {
                    if char != &'.' {
                        return acc;
                    }

                    if self.raster.iter().all(|row| row[x] == '.' || char == &'x') {
                        acc.push(x);
                    }

                    acc
                });

        for x in empty_colums.iter() {
            for y in 0..self.raster.len() {
                self.raster[y][*x] = 'x';
            }
        }
    }

    fn mark_empty_rows(self: &mut Self) {
        let empty_rows = self
            .raster
            .iter()
            .enumerate()
            .fold(Vec::new(), |mut acc, (y, row)| {
                if row.iter().all(|char| char == &'.' || char == &'x') {
                    acc.push(y);
                }

                acc
            });

        for y in empty_rows.iter() {
            self.raster[*y] = vec!['x'; self.raster[0].len()];
        }
    }
}

impl TryFrom<&Path> for TelescopeImage {
    type Error = Box<dyn Error>;

    fn try_from(file_path: &Path) -> Result<Self, Box<dyn Error>> {
        let file_contents = read_to_string(file_path)?;
        let mut raster: Vec<Vec<char>> = Vec::new();

        for line in file_contents.lines() {
            raster.push(line.chars().collect());
        }

        Ok(TelescopeImage {
            raster,
            galaxies: None,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_telescope_image_from_path() {
        assert_eq!(
            TelescopeImage::try_from(Path::new(
                format!("{}/fixtures/example", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_telescope_image()
        );
    }

    #[test]
    fn test_mark_empty_space() {
        let mut telescope_image = get_telescope_image();
        telescope_image.mark_empty_space();

        assert_eq!(telescope_image, get_marked_telescope_image());
    }

    #[test]
    fn test_calculate_distances() {
        assert_eq!(get_telescope_image().calculate_distances(2), 374);
        assert_eq!(get_telescope_image().calculate_distances(10), 1030);
        assert_eq!(get_telescope_image().calculate_distances(100), 8410);
    }

    fn get_telescope_image() -> TelescopeImage {
        TelescopeImage {
            raster: Vec::from([
                Vec::from(['.', '.', '.', '#', '.', '.', '.', '.', '.', '.']),
                Vec::from(['.', '.', '.', '.', '.', '.', '.', '#', '.', '.']),
                Vec::from(['#', '.', '.', '.', '.', '.', '.', '.', '.', '.']),
                Vec::from(['.', '.', '.', '.', '.', '.', '.', '.', '.', '.']),
                Vec::from(['.', '.', '.', '.', '.', '.', '#', '.', '.', '.']),
                Vec::from(['.', '#', '.', '.', '.', '.', '.', '.', '.', '.']),
                Vec::from(['.', '.', '.', '.', '.', '.', '.', '.', '.', '#']),
                Vec::from(['.', '.', '.', '.', '.', '.', '.', '.', '.', '.']),
                Vec::from(['.', '.', '.', '.', '.', '.', '.', '#', '.', '.']),
                Vec::from(['#', '.', '.', '.', '#', '.', '.', '.', '.', '.']),
            ]),
            galaxies: None,
        }
    }

    fn get_marked_telescope_image() -> TelescopeImage {
        TelescopeImage {
            raster: Vec::from([
                Vec::from(['.', '.', 'x', '#', '.', 'x', '.', '.', 'x', '.']),
                Vec::from(['.', '.', 'x', '.', '.', 'x', '.', '#', 'x', '.']),
                Vec::from(['#', '.', 'x', '.', '.', 'x', '.', '.', 'x', '.']),
                Vec::from(['x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x']),
                Vec::from(['.', '.', 'x', '.', '.', 'x', '#', '.', 'x', '.']),
                Vec::from(['.', '#', 'x', '.', '.', 'x', '.', '.', 'x', '.']),
                Vec::from(['.', '.', 'x', '.', '.', 'x', '.', '.', 'x', '#']),
                Vec::from(['x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x']),
                Vec::from(['.', '.', 'x', '.', '.', 'x', '.', '#', 'x', '.']),
                Vec::from(['#', '.', 'x', '.', '#', 'x', '.', '.', 'x', '.']),
            ]),
            galaxies: None,
        }
    }
}
