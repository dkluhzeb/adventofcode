use crate::TelescopeImage;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/11
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(TelescopeImage::try_from(file_path)?.calculate_distances(2))
}
