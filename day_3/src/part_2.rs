use crate::{get_gears, parse_lines};
use std::{error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/3#part2
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(sum_gears_from_lines(read_to_string(file_path)?.lines()))
}

/// Sums up the gear ratios
fn sum_gears_from_lines(lines: Lines) -> u32 {
    let data = parse_lines(lines);

    match data {
        (Some(parts), Some(symbols)) => match get_gears(parts, symbols) {
            Some(gears) => gears
                .iter()
                .fold(0, |acc, gear| acc + gear.0.value * gear.1.value),
            None => 0,
        },
        _ => 0,
    }
}
