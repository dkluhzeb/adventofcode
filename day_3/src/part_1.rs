use crate::{get_part_numbers, parse_lines};
use std::{error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/3
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(sum_parts_from_lines(read_to_string(file_path)?.lines()))
}

/// Sums up the part values
fn sum_parts_from_lines(lines: Lines) -> u32 {
    let data = parse_lines(lines);

    if data.0.is_none() {
        return 0;
    }

    match get_part_numbers(data.0.unwrap(), &data.1) {
        Some(parts) => parts.iter().fold(0, |acc, part| acc + part.value),
        None => 0,
    }
}
