use std::str::Lines;

pub mod part_1;
pub mod part_2;

/// This struct reprensents a number in the grid
#[derive(Clone, Debug, PartialEq)]
pub struct Number {
    /// Decimal value of the number
    value: u32,
    /// Grid coordinates of the number
    coords: Vec<(usize, usize)>,
}

/// This struct reprensents a symbol in the grid
#[derive(Debug, PartialEq)]
pub struct Symbol {
    /// Value of the symbol
    value: String,
    /// Grid coordinate of the symbol
    coords: (usize, usize),
}

/// Identifies parts by a given set of numbers and a given set of symbols
fn get_part_numbers(numbers: Vec<Number>, symbols: &Option<Vec<Symbol>>) -> Option<Vec<Number>> {
    if symbols.is_none() {
        return None;
    }

    let res: Vec<Number> = numbers
        .into_iter()
        .filter(|number| {
            let mut is_part = false;

            'identify_part: for coord in number.coords.iter() {
                let neighbors = generate_neighbors(coord);

                for neighbor in neighbors.iter() {
                    if symbols
                        .as_ref()
                        .unwrap()
                        .into_iter()
                        .find(|symbol| {
                            symbol.coords.0 == neighbor.0 && symbol.coords.1 == neighbor.1
                        })
                        .is_some()
                    {
                        is_part = true;
                        break 'identify_part;
                    }
                }
            }

            is_part
        })
        .collect();

    if res.len() > 0 {
        Some(res)
    } else {
        None
    }
}

/// Identifies gears by a given set of numbers and a given set of symbols
pub fn get_gears(parts: Vec<Number>, symbols: Vec<Symbol>) -> Option<Vec<(Number, Number)>> {
    let mut res: Vec<(Number, Number)> = Vec::new();

    let gear_symbols: Vec<Symbol> = symbols
        .into_iter()
        .filter(|symbol| symbol.value == String::from('*'))
        .collect();

    for gear_symbol in gear_symbols.iter() {
        let neighbors = generate_neighbors(&gear_symbol.coords);

        let adjacent_parts: Vec<Number> = parts
            .clone()
            .into_iter()
            .filter(|part| {
                for coord in part.coords.iter() {
                    for neighbor in neighbors.iter() {
                        if neighbor.0 == coord.0 && neighbor.1 == coord.1 {
                            return true;
                        }
                    }
                }

                return false;
            })
            .collect();

        if adjacent_parts.len() == 2 {
            res.push((
                adjacent_parts.first().unwrap().clone(),
                adjacent_parts.last().unwrap().clone(),
            ));
        }
    }

    if res.len() > 0 {
        Some(res)
    } else {
        None
    }
}

/// Generate the coordinates of all possible neighbors of a point
fn generate_neighbors(coords: &(usize, usize)) -> Vec<(usize, usize)> {
    let mut neighbors = Vec::from([
        (coords.0 + 1, coords.1 + 1),
        (coords.0, coords.1 + 1),
        (coords.0 + 1, coords.1),
    ]);

    if coords.0 > 0 && coords.1 > 0 {
        neighbors.push((coords.0 - 1, coords.1 - 1));
    }

    if coords.0 > 0 {
        neighbors.push((coords.0 - 1, coords.1 + 1));
        neighbors.push((coords.0 - 1, coords.1));
    }

    if coords.1 > 0 {
        neighbors.push((coords.0 + 1, coords.1 - 1));
        neighbors.push((coords.0, coords.1 - 1));
    }

    neighbors
}

/// Parses numbers and symbols from given lines
pub fn parse_lines(lines: Lines) -> (Option<Vec<Number>>, Option<Vec<Symbol>>) {
    let res = lines
        .enumerate()
        .fold((Vec::new(), Vec::new()), |mut acc, (y, line)| {
            match parse_line(line, y) {
                (Some(mut numbers), Some(mut symbols)) => {
                    acc.0.append(&mut numbers);
                    acc.1.append(&mut symbols);
                }
                (Some(mut numbers), None) => {
                    acc.0.append(&mut numbers);
                }
                (None, Some(mut symbols)) => {
                    acc.1.append(&mut symbols);
                }
                _ => {}
            }

            acc
        });

    (
        if res.0.len() > 0 { Some(res.0) } else { None },
        if res.1.len() > 0 { Some(res.1) } else { None },
    )
}

/// Parses numbers and symbols fram a single line
fn parse_line(str: &str, line_number: usize) -> (Option<Vec<Number>>, Option<Vec<Symbol>>) {
    let mut res_num: Vec<Number> = Vec::new();
    let mut res_symbols: Vec<Symbol> = Vec::new();

    let mut current_digits: String = String::new();
    let mut current_coords: Vec<(usize, usize)> = Vec::new();

    for (col_number, char) in str.chars().enumerate() {
        if char.is_ascii_digit() == false {
            if current_digits.len() > 0 {
                res_num.push(Number {
                    value: current_digits.parse::<u32>().unwrap(),
                    coords: current_coords,
                });

                current_digits = String::new();
                current_coords = Vec::new();
            }

            if char != '.' {
                res_symbols.push(Symbol {
                    value: String::from(char),
                    coords: (col_number, line_number),
                });
            }

            continue;
        }

        current_digits.push(char);
        current_coords.push((col_number, line_number));
    }

    if current_digits.len() > 0 {
        res_num.push(Number {
            value: current_digits.parse::<u32>().unwrap(),
            coords: current_coords,
        });
    }

    (
        if res_num.len() > 0 {
            Some(res_num)
        } else {
            None
        },
        if res_symbols.len() > 0 {
            Some(res_symbols)
        } else {
            None
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        let line = "467..114..";

        assert_eq!(
            parse_line(line, 1),
            (
                Some(Vec::from([
                    Number {
                        value: 467,
                        coords: Vec::from([(0, 1), (1, 1), (2, 1)]),
                    },
                    Number {
                        value: 114,
                        coords: Vec::from([(5, 1), (6, 1), (7, 1)]),
                    }
                ])),
                None
            )
        );
    }

    #[test]
    fn test_generate_neighbors() {
        assert_eq!(
            generate_neighbors(&(2, 2)),
            Vec::from([
                (3, 3),
                (2, 3),
                (3, 2),
                (1, 1),
                (1, 3),
                (1, 2),
                (3, 1),
                (2, 1)
            ])
        )
    }
}
