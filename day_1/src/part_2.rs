use std::{error::Error, collections::HashMap, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/1#part2
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(get_lines_sum(read_to_string(file_path)?.lines()))
}

/// Sums the value of given lines
fn get_lines_sum(lines: Lines) -> usize {
    lines
        .into_iter()
        .fold(0, |acc, line| acc + get_line_value(String::from(line)))
}

/// Calculates the value of a line
fn get_line_value(line: String) -> usize {
    let sanitized_line = sanitize_line(line);

    match (
        sanitized_line.chars().find(|char| char.is_ascii_digit()),
        sanitized_line.chars().rev().find(|char| char.is_ascii_digit()),
    ) {
        (Some(first_digit), Some(last_digit)) => format!("{}{}", first_digit, last_digit)
            .parse::<usize>()
            .unwrap(),
        _ => 0, // no digit in string.
    }
}

/// Sanitizes the digits of a line 
fn sanitize_line(line: String) -> String {
    let mut sanitized_line: String = String::new();

    let replace_map = HashMap::from([
        ("1", "one"),
        ("2", "two"),
        ("3", "three"),
        ("4", "four"),
        ("5", "five"),
        ("6", "six"),
        ("7", "seven"),
        ("8", "eight"),
        ("9", "nine"),
    ]);

    line.chars().enumerate().for_each(|(index, char)| {
        if char.is_ascii_digit() {
            sanitized_line.push(char);
        } else {
            replace_map.iter().for_each(|(to, from)| {
                match line.get(index..(index + from.len())) {
                    Some(compare) => {
                        if &compare == from {
                            sanitized_line.push_str(to);
                        }
                    },
                    None => {},
                }
            });
        }
    });
 
    sanitized_line
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_line_value() {
        assert_eq!(get_line_value(String::from("two1nine")), 29);
        assert_eq!(get_line_value(String::from("eightwothree")), 83);
        assert_eq!(get_line_value(String::from("abcone2threexyz")), 13);
        assert_eq!(get_line_value(String::from("xtwone3four")), 24);
        assert_eq!(get_line_value(String::from("4nineeightseven2")), 42);
        assert_eq!(get_line_value(String::from("zoneight234")), 14);
        assert_eq!(get_line_value(String::from("7pqrstsixteen")), 76);
    }

    #[test]
    fn test_sanitize_line() {
        assert_eq!(sanitize_line(String::from("two1nine")), String::from("219"));
        assert_eq!(sanitize_line(String::from("eightwothree")), String::from("823"));
        assert_eq!(sanitize_line(String::from("abcone2threexyz")), String::from("123"));
        assert_eq!(sanitize_line(String::from("xtwone3four")), String::from("2134"));
        assert_eq!(sanitize_line(String::from("4nineeightseven2")), String::from("49872"));
        assert_eq!(sanitize_line(String::from("zoneight234")), String::from("18234"));
        assert_eq!(sanitize_line(String::from("7pqrstsixteen")), String::from("76"));
    }

    #[test]
    fn test_get_lines_sum() {
        let lines: Lines = 
            "two1nine\r\neightwothree\r\nabcone2threexyz\r\nxtwone3four\r\n4nineeightseven2\r\nzoneight234\r\n7pqrstsixteen".lines();

        assert_eq!(get_lines_sum(lines), 281);
    }
}
