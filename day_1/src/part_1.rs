use std::{error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/1
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(get_lines_sum(read_to_string(file_path)?.lines()))
}

/// Sums the value of given lines
fn get_lines_sum(lines: Lines) -> usize {
    lines
        .into_iter()
        .fold(0, |acc, line| acc + get_line_value(String::from(line)))
}

/// Calculates the value of a line
fn get_line_value(line: String) -> usize {
    match (
        line.chars().find(|char| char.is_ascii_digit()),
        line.chars().rev().find(|char| char.is_ascii_digit()),
    ) {
        (Some(first_digit), Some(last_digit)) => format!("{}{}", first_digit, last_digit)
            .parse::<usize>()
            .unwrap(),
        _ => 0, // no digit in string
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_line_value() {
        assert_eq!(get_line_value(String::from("1abc2")), 12);
        assert_eq!(get_line_value(String::from("pqr3stu8vwx")), 38);
        assert_eq!(get_line_value(String::from("a1b2c3d4e5f")), 15);
        assert_eq!(get_line_value(String::from("treb7uchet")), 77);
    }

    #[test]
    fn test_get_lines_sum() {
        let lines: Lines = "1abc2\r\npqr3stu8vwx\r\na1b2c3d4e5f\r\ntreb7uchet".lines();

        assert_eq!(get_lines_sum(lines), 142);
    }
}
