# My answers for the advent of code puzzle 2023

[](https://adventofcode.com/)

## Puzzles
* [Day 1](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_1)
* [Day 2](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_2)
* [Day 3](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_3)
* [Day 4](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_4)
* [Day 5](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_5)
* [Day 6](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_6)
* [Day 7](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_7)
* [Day 8](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_8)
* [Day 9](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_9)
* [Day 10](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_10)
* [Day 11](https://gitlab.com/dkluhzeb/adventofcode/-/tree/main/day_11)

## Docs
* [Docs](https://adventofcode-dkluhzeb-9141cafd8ac41ac485640dc03e99e388ecce27da8.gitlab.io/doc/adventofcode/)
* [Benchmarks](https://adventofcode-dkluhzeb-9141cafd8ac41ac485640dc03e99e388ecce27da8.gitlab.io/benchmarks/report/)

## Binary
* [amd64](https://adventofcode-dkluhzeb-9141cafd8ac41ac485640dc03e99e388ecce27da8.gitlab.io/adventofcode)
