use std::{
    cmp::Ordering, collections::BTreeMap, error::Error, fs::read_to_string, iter::zip, path::Path,
};

pub mod part_1;
pub mod part_2;

#[derive(Debug, PartialEq)]
pub struct CamelCards {
    pub hands: Vec<Hand>,
    pub play_with_joker: bool,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Hand {
    pub cards: [Card; 5],
    pub bid: u16,
    pub value: HandValue,
}

#[derive(Clone, Debug, PartialEq)]
pub enum HandValue {
    FiveOfAKind(Card),
    FourOfAKind(Card),
    FullHouse((Card, Card)),
    ThreeOfAKind(Card),
    TwoPairs((Card, Card)),
    OnePair(Card),
    HighCard(Card),
}

#[derive(Clone, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum Card {
    A,
    K,
    Q,
    J,
    T,
    N9,
    N8,
    N7,
    N6,
    N5,
    N4,
    N3,
    N2,
}

impl CamelCards {
    pub fn new(hands: Vec<Hand>) -> Self {
        CamelCards {
            hands,
            play_with_joker: false,
        }
    }

    pub fn get_total_sum(self: &Self) -> usize {
        self.get_ranked()
            .into_iter()
            .enumerate()
            .fold(0, |mut acc, (place, hand)| {
                acc += (place + 1) * hand.bid as usize;

                acc
            })
    }

    pub fn get_ranked(self: &Self) -> Vec<Hand> {
        let mut ranked = self.hands.clone();

        ranked.sort_by(|a, b| {
            let a_sort_value = a.value.get_sort_value();
            let b_sort_value = b.value.get_sort_value();

            if a_sort_value > b_sort_value {
                return Ordering::Greater;
            }

            if a_sort_value < b_sort_value {
                return Ordering::Less;
            }

            let cards = zip(a.cards.clone(), b.cards.clone());

            for card in cards.into_iter() {
                let card_a_sort_value = card.0.get_sort_value(self.play_with_joker);
                let card_b_sort_value = card.1.get_sort_value(self.play_with_joker);

                if card_a_sort_value == card_b_sort_value {
                    continue;
                }

                if card_a_sort_value > card_b_sort_value {
                    return Ordering::Greater;
                }

                if card_a_sort_value < card_b_sort_value {
                    return Ordering::Less;
                }
            }

            Ordering::Equal
        });

        ranked
    }

    pub fn play_with_joker(self: &mut Self) {
        self.play_with_joker = true;
        self.hands = self
            .hands
            .to_vec()
            .into_iter()
            .map(|hand| hand.j_to_joker())
            .collect::<Vec<Hand>>();
    }
}

impl TryFrom<&Path> for CamelCards {
    type Error = Box<dyn Error>;

    fn try_from(file_path: &Path) -> Result<Self, Box<dyn Error>> {
        let file_contents = read_to_string(file_path)?;

        Ok(CamelCards::new(
            file_contents
                .lines()
                .filter(|line| line != &"")
                .map(|line| Ok(Hand::try_from(line)?))
                .collect::<Result<Vec<Hand>, Box<dyn Error>>>()?,
        ))
    }
}

impl HandValue {
    pub fn get_sort_value(self: &Self) -> u8 {
        match self {
            HandValue::FiveOfAKind(_) => 7,
            HandValue::FourOfAKind(_) => 6,
            HandValue::FullHouse(_) => 5,
            HandValue::ThreeOfAKind(_) => 4,
            HandValue::TwoPairs(_) => 3,
            HandValue::OnePair(_) => 2,
            HandValue::HighCard(_) => 1,
        }
    }

    pub fn from_cards(cards: &[Card; 5]) -> Self {
        let mut counts: BTreeMap<Card, u8> = BTreeMap::new();

        for card in cards.iter() {
            counts
                .entry(card.clone())
                .and_modify(|count| *count += 1)
                .or_insert(1);
        }

        for (card, count) in counts.clone() {
            if count == 5 {
                return HandValue::FiveOfAKind(card.clone());
            }
        }

        for (card, count) in counts.clone() {
            if count == 4 {
                return HandValue::FourOfAKind(card.clone());
            }
        }

        for (card, count) in counts.clone() {
            if count == 3 {
                for (card_pair, count_pair) in counts.clone() {
                    if count_pair == 2 {
                        return HandValue::FullHouse((card.clone(), card_pair.clone()));
                    }
                }

                return HandValue::ThreeOfAKind(card.clone());
            }
        }

        for (card, count) in counts.clone() {
            if count == 2 {
                for (card_pair, count_pair) in counts.clone() {
                    if count_pair == 2 && card != card_pair {
                        return HandValue::TwoPairs((card.clone(), card_pair.clone()));
                    }
                }

                return HandValue::OnePair(card.clone());
            }
        }

        let mut cards_vec: Vec<Card> = cards.to_vec();

        cards_vec.sort_by(|a, b| a.get_sort_value(false).cmp(&b.get_sort_value(false)));

        HandValue::HighCard(cards_vec.last().unwrap().clone())
    }
}

impl From<&[Card; 5]> for HandValue {
    fn from(cards: &[Card; 5]) -> Self {
        HandValue::from_cards(cards)
    }
}

impl Hand {
    pub fn new(cards: [Card; 5], bid: u16) -> Self {
        Hand {
            cards: cards.clone(),
            bid,
            value: HandValue::from(&cards),
        }
    }

    pub fn j_to_joker(self: Self) -> Self {
        let card_mutations: Vec<[Card; 5]> = Self::mutate_jokers(&self.cards, &mut Vec::new());

        if card_mutations.len() < 1 {
            return self;
        }

        let mut value_mutations = card_mutations
            .into_iter()
            .map(|mutation| HandValue::from(&mutation))
            .collect::<Vec<HandValue>>();

        value_mutations.sort_by(|a, b| b.get_sort_value().cmp(&a.get_sort_value()));

        Hand {
            cards: self.cards,
            bid: self.bid,
            value: value_mutations.first().unwrap().clone(),
        }
    }

    fn mutate_jokers(cards: &[Card; 5], card_mutations: &mut Vec<[Card; 5]>) -> Vec<[Card; 5]> {
        let mut current_card_mutations: Vec<[Card; 5]> = card_mutations.to_vec();
        let mut current_mutation: [Card; 5] = cards.clone();

        for (index, card) in cards.into_iter().enumerate() {
            if card != &Card::J {
                continue;
            }

            for mutation_card in [
                Card::A,
                Card::K,
                Card::Q,
                Card::T,
                Card::N9,
                Card::N8,
                Card::N7,
                Card::N6,
                Card::N5,
                Card::N4,
                Card::N3,
                Card::N2,
            ] {
                current_mutation[index] = mutation_card;
                card_mutations.push(current_mutation.clone());

                if current_mutation.iter().find(|j| j == &&Card::J).is_some() {
                    current_card_mutations
                        .append(&mut Self::mutate_jokers(&current_mutation, card_mutations));
                } else {
                    continue;
                }
            }
        }

        card_mutations.to_vec()
    }
}

impl TryFrom<&str> for Hand {
    type Error = Box<dyn Error>;

    fn try_from(str: &str) -> Result<Self, Box<dyn Error>> {
        let trimmed_line = str.trim();
        let mut split = trimmed_line.split(" ");

        if let (Some(cards_str), Some(bid_str)) = (split.next(), split.next()) {
            let cards = cards_str
                .as_bytes()
                .into_iter()
                .map(|byte| Ok(Card::try_from(byte)?))
                .collect::<Result<Vec<Card>, Box<dyn Error>>>()?;

            if cards.len() != 5 {
                return Err("Invalid hand size.".into());
            }

            Ok(Hand::new(
                cards.try_into().unwrap(),
                bid_str.parse::<u16>()?,
            ))
        } else {
            Err("Malformed input hand or bid not found.".into())
        }
    }
}

impl Card {
    pub fn get_sort_value(self: &Self, play_with_joker: bool) -> u8 {
        match self {
            Card::A => 14,
            Card::K => 13,
            Card::Q => 12,
            Card::J => {
                if play_with_joker {
                    1
                } else {
                    11
                }
            }
            Card::T => 10,
            Card::N9 => 9,
            Card::N8 => 8,
            Card::N7 => 7,
            Card::N6 => 6,
            Card::N5 => 5,
            Card::N4 => 4,
            Card::N3 => 3,
            Card::N2 => 2,
        }
    }
}

impl TryFrom<&u8> for Card {
    type Error = Box<dyn Error>;

    fn try_from(byte: &u8) -> Result<Self, Box<dyn Error>> {
        match byte {
            &0x41 => Ok(Card::A),
            &0x4b => Ok(Card::K),
            &0x51 => Ok(Card::Q),
            &0x4a => Ok(Card::J),
            &0x54 => Ok(Card::T),
            &0x39 => Ok(Card::N9),
            &0x38 => Ok(Card::N8),
            &0x37 => Ok(Card::N7),
            &0x36 => Ok(Card::N6),
            &0x35 => Ok(Card::N5),
            &0x34 => Ok(Card::N4),
            &0x33 => Ok(Card::N3),
            &0x32 => Ok(Card::N2),
            _ => Err("Unable to parse card.".into()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_camel_cards_from_path() {
        assert_eq!(
            CamelCards::try_from(Path::new(
                format!("{}/fixtures/example", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_camel_cards()
        );
    }

    #[test]
    fn test_hand_from_str() {
        assert_eq!(
            Hand::try_from("32T3K 765").unwrap(),
            Hand {
                cards: [Card::N3, Card::N2, Card::T, Card::N3, Card::K],
                bid: 765,
                value: HandValue::OnePair(Card::N3)
            }
        )
    }

    #[test]
    fn test_card_from_byte() {
        assert_eq!(
            Card::try_from("A".as_bytes().first().unwrap()).unwrap(),
            Card::A
        );
        assert_eq!(
            Card::try_from("K".as_bytes().first().unwrap()).unwrap(),
            Card::K
        );
        assert_eq!(
            Card::try_from("Q".as_bytes().first().unwrap()).unwrap(),
            Card::Q
        );
        assert_eq!(
            Card::try_from("T".as_bytes().first().unwrap()).unwrap(),
            Card::T
        );
        assert_eq!(
            Card::try_from("6".as_bytes().first().unwrap()).unwrap(),
            Card::N6
        );
        assert_eq!(
            Card::try_from("5".as_bytes().first().unwrap()).unwrap(),
            Card::N5
        );
        assert_eq!(
            Card::try_from("4".as_bytes().first().unwrap()).unwrap(),
            Card::N4
        );
        assert_eq!(
            Card::try_from("3".as_bytes().first().unwrap()).unwrap(),
            Card::N3
        );
        assert_eq!(
            Card::try_from("2".as_bytes().first().unwrap()).unwrap(),
            Card::N2
        );
        assert!(Card::try_from("X".as_bytes().first().unwrap()).is_err());
    }

    #[test]
    fn test_get_ranked_hands() {
        assert_eq!(get_camel_cards().get_ranked(), get_ranked_hands());
    }

    #[test]
    fn test_get_total_sum() {
        assert_eq!(get_camel_cards().get_total_sum(), 6440);
    }

    #[test]
    fn test_get_total_sum_with_joker() {
        let mut camel_cards = get_camel_cards();

        camel_cards.play_with_joker();

        assert_eq!(camel_cards.get_total_sum(), 5905);
    }

    fn get_camel_cards() -> CamelCards {
        CamelCards {
            hands: Vec::from([
                Hand {
                    cards: [Card::N3, Card::N2, Card::T, Card::N3, Card::K],
                    bid: 765,
                    value: HandValue::OnePair(Card::N3),
                },
                Hand {
                    cards: [Card::T, Card::N5, Card::N5, Card::J, Card::N5],
                    bid: 684,
                    value: HandValue::ThreeOfAKind(Card::N5),
                },
                Hand {
                    cards: [Card::K, Card::K, Card::N6, Card::N7, Card::N7],
                    bid: 28,
                    value: HandValue::TwoPairs((Card::K, Card::N7)),
                },
                Hand {
                    cards: [Card::K, Card::T, Card::J, Card::J, Card::T],
                    bid: 220,
                    value: HandValue::TwoPairs((Card::J, Card::T)),
                },
                Hand {
                    cards: [Card::Q, Card::Q, Card::Q, Card::J, Card::A],
                    bid: 483,
                    value: HandValue::ThreeOfAKind(Card::Q),
                },
            ]),
            play_with_joker: false,
        }
    }

    fn get_ranked_hands() -> Vec<Hand> {
        Vec::from([
            Hand {
                cards: [Card::N3, Card::N2, Card::T, Card::N3, Card::K],
                bid: 765,
                value: HandValue::OnePair(Card::N3),
            },
            Hand {
                cards: [Card::K, Card::T, Card::J, Card::J, Card::T],
                bid: 220,
                value: HandValue::TwoPairs((Card::J, Card::T)),
            },
            Hand {
                cards: [Card::K, Card::K, Card::N6, Card::N7, Card::N7],
                bid: 28,
                value: HandValue::TwoPairs((Card::K, Card::N7)),
            },
            Hand {
                cards: [Card::T, Card::N5, Card::N5, Card::J, Card::N5],
                bid: 684,
                value: HandValue::ThreeOfAKind(Card::N5),
            },
            Hand {
                cards: [Card::Q, Card::Q, Card::Q, Card::J, Card::A],
                bid: 483,
                value: HandValue::ThreeOfAKind(Card::Q),
            },
        ])
    }
}
