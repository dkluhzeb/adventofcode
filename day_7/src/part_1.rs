use crate::CamelCards;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/7
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(CamelCards::try_from(file_path)?.get_total_sum())
}
