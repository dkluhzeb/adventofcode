use crate::CamelCards;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/7#part_2
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    let mut camel_cards = CamelCards::try_from(file_path)?;

    camel_cards.play_with_joker();

    Ok(camel_cards.get_total_sum())
}
