use std::error::Error;

pub fn lcm_from_vec(numbers: Vec<usize>) -> Result<usize, Box<dyn Error>> {
    if numbers.len() < 2 {
        return Err("At least two numbers needed.".into());
    }

    let mut prev_lcm: Option<usize> = None;

    for number in numbers {
        if prev_lcm.is_none() {
            prev_lcm = Some(number);

            continue;
        }

        prev_lcm = Some(lcm(prev_lcm.unwrap(), number));
    }

    match prev_lcm {
        Some(n) => Ok(n),
        None => Err("Unexpected".into()),
    }
}

fn lcm(first: usize, second: usize) -> usize {
    first * second / gcd(first, second)
}

fn gcd(first: usize, second: usize) -> usize {
    let mut max = first;
    let mut min = second;
    if min > max {
        let val = max;
        max = min;
        min = val;
    }

    loop {
        let res = max % min;
        if res == 0 {
            return min;
        }

        max = min;
        min = res;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lcm_from_vec() {
        assert_eq!(lcm_from_vec(Vec::from([12, 15, 75])).unwrap(), 300);
    }

    #[test]
    fn test_lcm() {
        assert_eq!(lcm(12, 15), 60);
    }

    #[test]
    fn test_gcd() {
        assert_eq!(gcd(12, 15), 3);
    }
}
