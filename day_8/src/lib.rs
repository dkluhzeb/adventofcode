use crate::utils::lcm_from_vec;
use std::{collections::BTreeMap, error::Error, fs::read_to_string, path::Path};

pub mod part_1;
pub mod part_2;
pub mod utils;

#[derive(Debug, PartialEq)]
pub enum Direction {
    L,
    R,
}

#[derive(Debug, PartialEq)]
pub struct Maps {
    directions: Vec<Direction>,
    network: BTreeMap<String, (String, String)>,
}

impl TryFrom<&char> for Direction {
    type Error = Box<dyn Error>;

    fn try_from(direction_char: &char) -> Result<Self, Box<dyn Error>> {
        match direction_char {
            &'L' => Ok(Direction::L),
            &'R' => Ok(Direction::R),
            _ => Err("Unknown direction.".into()),
        }
    }
}

impl Maps {
    pub fn count_steps_to_target(self: &Self) -> Result<usize, Box<dyn Error>> {
        self.steps_to_target(&String::from("AAA"), 0, 0)
    }

    fn steps_to_target(
        self: &Self,
        node_key: &String,
        count: usize,
        direction_index: usize,
    ) -> Result<usize, Box<dyn Error>> {
        match self.network.get(node_key) {
            Some(node) => {
                let next_key = match &self.directions[direction_index] {
                    &Direction::L => &node.0,
                    &Direction::R => &node.1,
                };

                if next_key.as_str() == "ZZZ" {
                    Ok(count + 1)
                } else {
                    let mut next_direction_index = direction_index + 1;

                    if next_direction_index > self.directions.len() - 1 {
                        next_direction_index = 0;
                    }

                    self.steps_to_target(next_key, count + 1, next_direction_index)
                }
            }
            None => Err(format!("Node {} not found.", node_key).into()),
        }
    }

    pub fn count_ghost_steps_to_target(self: &Self) -> Result<usize, Box<dyn Error>> {
        self.ghost_steps_to_target_lcm(
            self.network
                .clone()
                .into_keys()
                .filter(|key| key.ends_with("A"))
                .collect(),
        )
    }

    pub fn ghost_steps_to_target_lcm(
        self: &Self,
        start_nodes: Vec<String>,
    ) -> Result<usize, Box<dyn Error>> {
        lcm_from_vec(
            start_nodes
                .into_iter()
                .map(|node| self.ghost_steps_to_target(node))
                .collect(),
        )
    }

    pub fn ghost_steps_to_target(self: &Self, start_node: String) -> usize {
        let mut current_node: String = start_node.clone();
        let mut direction_index: usize = 0;
        let mut count: usize = 0;

        loop {
            if current_node.ends_with("Z") {
                break;
            }

            count = count + 1;

            let next_node = self
                .network
                .get(&current_node)
                .expect("Missing child node.");

            current_node = match &self.directions[direction_index] {
                &Direction::L => next_node.clone().0,
                &Direction::R => next_node.clone().1,
            };

            direction_index = direction_index + 1;

            if direction_index > self.directions.len() - 1 {
                direction_index = 0;
            }
        }

        count
    }
}

impl TryFrom<&Path> for Maps {
    type Error = Box<dyn Error>;

    fn try_from(file_path: &Path) -> Result<Self, Box<dyn Error>> {
        let file_contents = read_to_string(file_path)?;
        let mut is_direction_header = true;
        let mut directions: Vec<Direction> = Vec::new();
        let mut network: BTreeMap<String, (String, String)> = BTreeMap::new();

        for line in file_contents.lines() {
            let trimmed_line = line.trim();

            if trimmed_line == "" {
                is_direction_header = false;
                continue;
            }

            if is_direction_header {
                for char in line.chars() {
                    directions.push(Direction::try_from(&char)?);
                }
            } else {
                let mut key_split = trimmed_line.split("=");
                let key = key_split.next().expect("No key found.").trim().to_string();
                let mut value_split = key_split.next().expect("No values found.").split(",");
                let l = value_split.next().expect("No l value found.").trim()[1..].to_string();
                let r = value_split.next().expect("No r value found.").trim()[..3].to_string();

                network.insert(key, (l, r));
            }
        }

        Ok(Maps {
            directions,
            network,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_direction_from_char() {
        let l: &char = &'L';
        let r: &char = &'R';
        let x: &char = &'x';

        assert_eq!(Direction::try_from(l).unwrap(), Direction::L);
        assert_eq!(Direction::try_from(r).unwrap(), Direction::R);
        assert_eq!(Direction::try_from(x).is_err(), true);
    }

    #[test]
    fn test_maps_from_path() {
        assert_eq!(
            Maps::try_from(Path::new(
                format!("{}/fixtures/example", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_maps()
        );

        assert_eq!(
            Maps::try_from(Path::new(
                format!("{}/fixtures/example_2", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_maps_2()
        );

        assert_eq!(
            Maps::try_from(Path::new(
                format!("{}/fixtures/example_3", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_maps_3()
        );
    }

    #[test]
    fn test_count_steps_to_target() {
        assert_eq!(get_maps().count_steps_to_target().unwrap(), 2);
        assert_eq!(get_maps_2().count_steps_to_target().unwrap(), 6);
    }

    #[test]
    fn test_count_ghost_steps_to_target() {
        assert_eq!(get_maps_3().count_ghost_steps_to_target().unwrap(), 6);
    }

    fn get_maps() -> Maps {
        Maps {
            directions: Vec::from([Direction::R, Direction::L]),
            network: BTreeMap::from([
                (
                    String::from("AAA"),
                    (String::from("BBB"), String::from("CCC")),
                ),
                (
                    String::from("BBB"),
                    (String::from("DDD"), String::from("EEE")),
                ),
                (
                    String::from("CCC"),
                    (String::from("ZZZ"), String::from("GGG")),
                ),
                (
                    String::from("DDD"),
                    (String::from("DDD"), String::from("DDD")),
                ),
                (
                    String::from("EEE"),
                    (String::from("EEE"), String::from("EEE")),
                ),
                (
                    String::from("GGG"),
                    (String::from("GGG"), String::from("GGG")),
                ),
                (
                    String::from("ZZZ"),
                    (String::from("ZZZ"), String::from("ZZZ")),
                ),
            ]),
        }
    }

    fn get_maps_2() -> Maps {
        Maps {
            directions: Vec::from([Direction::L, Direction::L, Direction::R]),
            network: BTreeMap::from([
                (
                    String::from("AAA"),
                    (String::from("BBB"), String::from("BBB")),
                ),
                (
                    String::from("BBB"),
                    (String::from("AAA"), String::from("ZZZ")),
                ),
                (
                    String::from("ZZZ"),
                    (String::from("ZZZ"), String::from("ZZZ")),
                ),
            ]),
        }
    }

    fn get_maps_3() -> Maps {
        Maps {
            directions: Vec::from([Direction::L, Direction::R]),
            network: BTreeMap::from([
                (
                    String::from("11A"),
                    (String::from("11B"), String::from("XXX")),
                ),
                (
                    String::from("11B"),
                    (String::from("XXX"), String::from("11Z")),
                ),
                (
                    String::from("11Z"),
                    (String::from("11B"), String::from("XXX")),
                ),
                (
                    String::from("22A"),
                    (String::from("22B"), String::from("XXX")),
                ),
                (
                    String::from("22B"),
                    (String::from("22C"), String::from("22C")),
                ),
                (
                    String::from("22C"),
                    (String::from("22Z"), String::from("22Z")),
                ),
                (
                    String::from("22Z"),
                    (String::from("22B"), String::from("22B")),
                ),
                (
                    String::from("XXX"),
                    (String::from("XXX"), String::from("XXX")),
                ),
            ]),
        }
    }
}
