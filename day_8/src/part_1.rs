use crate::Maps;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/8
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(Maps::try_from(file_path)?.count_steps_to_target()?)
}
