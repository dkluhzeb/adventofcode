use crate::{parse_lines, Card};
use std::{collections::HashMap, error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/4#part2
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(sum_total_cards(read_to_string(file_path)?.lines())?)
}

/// Sums up all cards scratchable cards by given lines
fn sum_total_cards(lines: Lines) -> Result<u32, &'static str> {
    let original_cards = parse_lines(lines)?;
    let original_cards_map = cards_to_count_map(original_cards.clone());

    Ok(scratch_cards(
        original_cards.iter().map(|card| card.id).collect(),
        &mut 0,
        original_cards_map,
    ))
}

/// Sums up all cards scratchable cards by given cards an results count map
fn scratch_cards(cards: Vec<u32>, count: &mut u32, count_map: HashMap<u32, u32>) -> u32 {
    let mut new_cards: Vec<u32> = Vec::new();

    for id in cards.into_iter() {
        let wins = count_map.get(&id).unwrap();

        *count = *count + 1;

        if *wins == 0 {
            continue;
        }

        for new_card in (id + 1)..(id + 1 + wins) {
            if count_map.contains_key(&new_card) {
                new_cards.push(new_card);
            }
        }
    }

    if new_cards.len() > 0 {
        scratch_cards(new_cards, count, count_map)
    } else {
        *count
    }
}

/// Generates a results count map
fn cards_to_count_map(cards: Vec<Card>) -> HashMap<u32, u32> {
    let mut card_map: HashMap<u32, u32> = HashMap::new();

    for card in cards {
        card_map.insert(card.id, card.get_matching_count());
    }

    card_map
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum_total_cards() {
        let lines = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\r\nCard 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\r\nCard 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\r\nCard 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\r\nCard 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\r\nCard 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11".lines();

        assert_eq!(sum_total_cards(lines), Ok(30));
    }
}
