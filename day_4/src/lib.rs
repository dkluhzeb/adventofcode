use std::{num::ParseIntError, str::Lines};

pub mod part_1;
pub mod part_2;

/// This struct reprensents a card
#[derive(Clone, Debug, PartialEq)]
pub struct Card {
    /// Id of the card
    id: u32,
    /// The winning numbers of this card
    winning_numbers: Vec<u32>,
    /// The scratched numbers of this card
    numbers: Vec<u32>,
}

/// TryFrom trait implementation for a card
/// Required format: Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
impl TryFrom<&str> for Card {
    /// Parse error type for this implementation
    type Error = &'static str;

    /// Parse a Card from a string slice
    fn try_from(str: &str) -> Result<Self, Self::Error> {
        let mut parts = str.split([':', '|']).collect::<Vec<&str>>().into_iter();

        match (
            parse_id(parts.next()),
            parse_numbers(parts.next()),
            parse_numbers(parts.next()),
        ) {
            (Some(id), Some(winning_numbers), Some(numbers)) => Ok(Card {
                id,
                winning_numbers,
                numbers,
            }),
            _ => Err("Unable to parse Card."),
        }
    }
}

/// Method implementations for a Card
impl Card {
    /// Calculates the score (points) for a Card
    pub fn calculate_points(self: Self) -> u32 {
        let mut res = 0;

        for number in self.numbers {
            if self
                .winning_numbers
                .iter()
                .find(|winning_number| winning_number == &&number)
                .is_none()
            {
                continue;
            }

            if res == 0 {
                res = 1;
            } else {
                res = res * 2;
            }
        }

        res
    }

    /// Calculates the count of scratched winning numbers of a card
    pub fn get_matching_count(self: Self) -> u32 {
        let mut res = 0;

        for number in self.numbers {
            if self
                .winning_numbers
                .iter()
                .find(|winning_number| winning_number == &&number)
                .is_none()
            {
                continue;
            }

            res += 1;
        }

        res
    }
}

/// Parses the id from a string slice
/// Required format: Card 1
fn parse_id(str: Option<&str>) -> Option<u32> {
    match str?
        .trim()
        .split(" ")
        .filter(|char| char != &"") // remove empty kinds (multiple whitespace delimiters)
        .nth(1)?
        .parse::<u32>()
    {
        Ok(id) => Some(id),
        Err(..) => None,
    }
}

/// Parses the numbers from a string slice
/// Required format: 41 48 83 86 17
fn parse_numbers(str: Option<&str>) -> Option<Vec<u32>> {
    let res: Result<Vec<u32>, ParseIntError> = str?
        .trim()
        .split(" ")
        .filter(|char| char != &"") // remove empty kinds (multiple whitespace delimiters)
        .map(|number| number.trim().parse::<u32>())
        .collect();

    match res {
        Ok(numbers) => Some(numbers),
        Err(..) => None,
    }
}

/// Parses lines to Cards
pub fn parse_lines(lines: Lines) -> Result<Vec<Card>, &'static str> {
    lines.map(|line| Card::try_from(line)).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_id() {
        assert_eq!(parse_id(Some("Card 1")), Some(1));
        assert_eq!(parse_id(Some("2")), None);
        assert_eq!(parse_id(Some("Card2")), None);
    }

    #[test]
    fn test_parse_numbers() {
        assert_eq!(
            parse_numbers(Some("41 48 83 86 17  1")),
            Some(Vec::from([41, 48, 83, 86, 17, 1]))
        );
        assert_eq!(parse_numbers(Some("41 48 83 : 86 17")), None);
    }

    #[test]
    fn test_card_from_str() {
        assert_eq!(
            Card::try_from("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"),
            Ok(Card {
                id: 3,
                winning_numbers: Vec::from([1, 21, 53, 59, 44]),
                numbers: Vec::from([69, 82, 63, 72, 16, 21, 14, 1])
            })
        );

        assert_eq!(
            Card::try_from("Card4:  1 21 53#59 44 | 69 82 63 72 16 21 14  1"),
            Err("Unable to parse Card.")
        );
    }

    #[test]
    fn test_parse_lines() {
        let lines = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\r\nCard 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\r\nCard 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1".lines();

        assert_eq!(
            parse_lines(lines),
            Ok(Vec::from([
                Card {
                    id: 1,
                    winning_numbers: Vec::from([41, 48, 83, 86, 17]),
                    numbers: Vec::from([83, 86, 6, 31, 17, 9, 48, 53])
                },
                Card {
                    id: 2,
                    winning_numbers: Vec::from([13, 32, 20, 16, 61]),
                    numbers: Vec::from([61, 30, 68, 82, 17, 32, 24, 19])
                },
                Card {
                    id: 3,
                    winning_numbers: Vec::from([1, 21, 53, 59, 44]),
                    numbers: Vec::from([69, 82, 63, 72, 16, 21, 14, 1])
                }
            ]))
        );
    }

    #[test]
    fn test_calculate_points() {
        assert_eq!(
            Card {
                id: 1,
                winning_numbers: Vec::from([41, 48, 83, 86, 17]),
                numbers: Vec::from([83, 86, 6, 31, 17, 9, 48, 53])
            }
            .calculate_points(),
            8
        );
        assert_eq!(
            Card {
                id: 2,
                winning_numbers: Vec::from([13, 32, 20, 16, 61]),
                numbers: Vec::from([61, 30, 68, 82, 17, 32, 24, 19])
            }
            .calculate_points(),
            2
        );
        assert_eq!(
            Card {
                id: 3,
                winning_numbers: Vec::from([1, 21, 53, 59, 44]),
                numbers: Vec::from([69, 82, 63, 72, 16, 21, 14, 1])
            }
            .calculate_points(),
            2
        );
    }

    #[test]
    fn test_get_matching_count() {
        assert_eq!(
            Card {
                id: 1,
                winning_numbers: Vec::from([41, 48, 83, 86, 17]),
                numbers: Vec::from([83, 86, 6, 31, 17, 9, 48, 53])
            }
            .get_matching_count(),
            4
        );
        assert_eq!(
            Card {
                id: 2,
                winning_numbers: Vec::from([13, 32, 20, 16, 61]),
                numbers: Vec::from([61, 30, 68, 82, 17, 32, 24, 19])
            }
            .get_matching_count(),
            2
        );
        assert_eq!(
            Card {
                id: 3,
                winning_numbers: Vec::from([1, 21, 53, 59, 44]),
                numbers: Vec::from([69, 82, 63, 72, 16, 21, 14, 1])
            }
            .get_matching_count(),
            2
        );
    }
}
