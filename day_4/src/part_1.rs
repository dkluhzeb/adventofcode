use crate::parse_lines;
use std::{error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/4
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(sum_card_points(read_to_string(file_path)?.lines())?)
}

/// Sums up the calculated points of all cards
fn sum_card_points(lines: Lines) -> Result<u32, &'static str> {
    parse_lines(lines)?.into_iter().fold(Ok(0), |acc, card| {
        Ok(acc.unwrap() + card.calculate_points())
    })
}
