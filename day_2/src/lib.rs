pub mod part_1;
pub mod part_2;

/// This struct reprensents a set of cubes
#[derive(Debug, PartialEq)]
struct ColorCount {
    /// Amount of red cubes
    red: u32,
    /// Amount of green cubes
    green: u32,
    /// Amount of blue cubes
    blue: u32,
}

/// From trait implementation for a ColorCount cube set
/// Required Format: " 8 green, 6 blue, 20 red"
impl From<&str> for ColorCount {
    /// Parse a ColorCount set from a string slice
    fn from(str: &str) -> Self {
        let mut turn = ColorCount {
            red: 0,
            green: 0,
            blue: 0,
        };

        str.trim().split(",").for_each(|turn_str| {
            let color = turn_str.trim().split(" ");
            let key = color.clone().last().unwrap();
            let value = color.clone().nth(0).unwrap().parse::<u32>().unwrap();

            match key {
                "red" => turn.red = value,
                "green" => turn.green = value,
                "blue" => turn.blue = value,
                _ => {}
            }
        });

        turn
    }
}

/// Method implementations for a ColorCount set
impl ColorCount {
    /// Calculates the "power" of a ColorCount set
    pub fn get_power(self: &Self) -> u32 {
        self.red * self.green * self.blue
    }
}

/// This struct reprensents a game
#[derive(Debug, PartialEq)]
struct Game {
    /// Id of the game
    id: u32,
    /// The ColorCount set for each turn
    turns: Vec<ColorCount>,
}

/// From trait implementation for a Game
impl From<&str> for Game {
    /// Parse a Game set from a string slice
    fn from(str: &str) -> Self {
        let game_split = str.split(":");
        let turns = game_split.clone().last().unwrap().split(";");

        let id: u32 = game_split
            .clone()
            .nth(0)
            .unwrap()
            .split(" ")
            .last()
            .unwrap()
            .parse::<u32>()
            .unwrap();

        Game {
            id,
            turns: turns.map(|turn| ColorCount::from(turn)).collect(),
        }
    }
}

/// Method implementations for a Game
impl Game {
    /// Calculates if is a game is possible by a given bag ColorCount set.
    pub fn is_possible(self: &Self, bag: &ColorCount) -> bool {
        let mut valid: bool = true;

        self.turns.iter().for_each(|turn| {
            if turn.red > bag.red || turn.green > bag.green || turn.blue > bag.blue {
                valid = false;
            }
        });

        valid
    }

    /// Calculates the minimal bag size for a game.
    pub fn get_bag_minimum(self: &Self) -> ColorCount {
        self.turns.iter().fold(
            ColorCount {
                red: 0,
                green: 0,
                blue: 0,
            },
            |mut acc, turn| {
                if turn.red > acc.red {
                    acc.red = turn.red;
                }

                if turn.green > acc.green {
                    acc.green = turn.green;
                }

                if turn.blue > acc.blue {
                    acc.blue = turn.blue;
                }
                acc
            },
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_turn_from_string() {
        assert_eq!(
            ColorCount::from(" 3 blue, 4 red"),
            ColorCount {
                red: 4,
                green: 0,
                blue: 3,
            }
        );
        assert_eq!(
            ColorCount::from(" 1 red, 2 green, 6 blue"),
            ColorCount {
                red: 1,
                green: 2,
                blue: 6,
            }
        );
        assert_eq!(
            ColorCount::from(" 2 green"),
            ColorCount {
                red: 0,
                green: 2,
                blue: 0,
            }
        );
    }

    #[test]
    fn test_game_from_string() {
        assert_eq!(
            Game::from("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
            Game {
                id: 1,
                turns: Vec::from([
                    ColorCount {
                        red: 4,
                        green: 0,
                        blue: 3
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 6
                    },
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 0
                    },
                ])
            }
        );
        assert_eq!(
            Game::from("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"),
            Game {
                id: 2,
                turns: Vec::from([
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 3,
                        blue: 4
                    },
                    ColorCount {
                        red: 0,
                        green: 1,
                        blue: 1
                    },
                ])
            }
        );
        assert_eq!(
            Game::from("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"),
            Game {
                id: 3,
                turns: Vec::from([
                    ColorCount {
                        red: 20,
                        green: 8,
                        blue: 6
                    },
                    ColorCount {
                        red: 4,
                        green: 13,
                        blue: 5
                    },
                    ColorCount {
                        red: 1,
                        green: 5,
                        blue: 0
                    },
                ])
            }
        );
        assert_eq!(
            Game::from("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"),
            Game {
                id: 4,
                turns: Vec::from([
                    ColorCount {
                        red: 3,
                        green: 1,
                        blue: 6
                    },
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 0
                    },
                    ColorCount {
                        red: 14,
                        green: 3,
                        blue: 15
                    },
                ])
            }
        );
        assert_eq!(
            Game::from("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"),
            Game {
                id: 5,
                turns: Vec::from([
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 2
                    },
                ])
            }
        );
    }

    #[test]
    fn test_is_possible() {
        let bag = &ColorCount {
            red: 12,
            green: 13,
            blue: 14,
        };

        assert_eq!(
            Game {
                id: 1,
                turns: Vec::from([
                    ColorCount {
                        red: 4,
                        green: 0,
                        blue: 3
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 6
                    },
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 0
                    },
                ])
            }
            .is_possible(bag),
            true
        );

        assert_eq!(
            Game {
                id: 2,
                turns: Vec::from([
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 3,
                        blue: 4
                    },
                    ColorCount {
                        red: 0,
                        green: 1,
                        blue: 1
                    },
                ])
            }
            .is_possible(bag),
            true
        );

        assert_eq!(
            Game {
                id: 3,
                turns: Vec::from([
                    ColorCount {
                        red: 20,
                        green: 8,
                        blue: 6
                    },
                    ColorCount {
                        red: 4,
                        green: 13,
                        blue: 5
                    },
                    ColorCount {
                        red: 1,
                        green: 5,
                        blue: 0
                    },
                ])
            }
            .is_possible(bag),
            false
        );

        assert_eq!(
            Game {
                id: 4,
                turns: Vec::from([
                    ColorCount {
                        red: 3,
                        green: 1,
                        blue: 6
                    },
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 0
                    },
                    ColorCount {
                        red: 14,
                        green: 3,
                        blue: 15
                    },
                ])
            }
            .is_possible(bag),
            false
        );

        assert_eq!(
            Game {
                id: 5,
                turns: Vec::from([
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 2
                    },
                ])
            }
            .is_possible(bag),
            true
        );
    }

    #[test]
    fn test_get_bag_minimum() {
        assert_eq!(
            Game {
                id: 1,
                turns: Vec::from([
                    ColorCount {
                        red: 4,
                        green: 0,
                        blue: 3
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 6
                    },
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 0
                    },
                ])
            }
            .get_bag_minimum(),
            ColorCount {
                red: 4,
                green: 2,
                blue: 6
            }
        );

        assert_eq!(
            Game {
                id: 2,
                turns: Vec::from([
                    ColorCount {
                        red: 0,
                        green: 2,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 3,
                        blue: 4
                    },
                    ColorCount {
                        red: 0,
                        green: 1,
                        blue: 1
                    },
                ])
            }
            .get_bag_minimum(),
            ColorCount {
                red: 1,
                green: 3,
                blue: 4
            }
        );

        assert_eq!(
            Game {
                id: 3,
                turns: Vec::from([
                    ColorCount {
                        red: 20,
                        green: 8,
                        blue: 6
                    },
                    ColorCount {
                        red: 4,
                        green: 13,
                        blue: 5
                    },
                    ColorCount {
                        red: 1,
                        green: 5,
                        blue: 0
                    },
                ])
            }
            .get_bag_minimum(),
            ColorCount {
                red: 20,
                green: 13,
                blue: 6
            }
        );

        assert_eq!(
            Game {
                id: 4,
                turns: Vec::from([
                    ColorCount {
                        red: 3,
                        green: 1,
                        blue: 6
                    },
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 0
                    },
                    ColorCount {
                        red: 14,
                        green: 3,
                        blue: 15
                    },
                ])
            }
            .get_bag_minimum(),
            ColorCount {
                red: 14,
                green: 3,
                blue: 15
            }
        );

        assert_eq!(
            Game {
                id: 5,
                turns: Vec::from([
                    ColorCount {
                        red: 6,
                        green: 3,
                        blue: 1
                    },
                    ColorCount {
                        red: 1,
                        green: 2,
                        blue: 2
                    },
                ])
            }
            .get_bag_minimum(),
            ColorCount {
                red: 6,
                green: 3,
                blue: 2
            }
        );
    }

    #[test]
    fn test_get_power() {
        assert_eq!(
            ColorCount {
                red: 4,
                green: 2,
                blue: 6
            }
            .get_power(),
            48
        );
        assert_eq!(
            ColorCount {
                red: 1,
                green: 3,
                blue: 4
            }
            .get_power(),
            12
        );
        assert_eq!(
            ColorCount {
                red: 20,
                green: 13,
                blue: 6
            }
            .get_power(),
            1560
        );
        assert_eq!(
            ColorCount {
                red: 14,
                green: 3,
                blue: 15
            }
            .get_power(),
            630
        );
        assert_eq!(
            ColorCount {
                red: 6,
                green: 3,
                blue: 2
            }
            .get_power(),
            36
        );
    }
}
