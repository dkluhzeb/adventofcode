use crate::Game;
use std::{convert::From, error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/2#part2
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(get_minimal_bag_power_sum(
        read_to_string(file_path)?.lines(),
    ))
}

/// Sums the power of all bag minimums.
fn get_minimal_bag_power_sum(lines: Lines) -> u32 {
    lines.fold(0, |mut acc, line| {
        acc += Game::from(line).get_bag_minimum().get_power();

        acc
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_minimal_bag_power_sum() {
        let lines: Lines = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\r\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\r\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\r\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\r\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
            .lines();

        assert_eq!(get_minimal_bag_power_sum(lines), 2286);
    }
}
