use crate::{ColorCount, Game};
use std::{convert::From, error::Error, fs::read_to_string, path::Path, str::Lines};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/2
pub fn run(file_path: &Path) -> Result<u32, Box<dyn Error>> {
    Ok(get_possible_id_sum(
        read_to_string(file_path)?.lines(),
        &ColorCount {
            red: 12,
            green: 13,
            blue: 14,
        },
    ))
}

/// Sums the ids on all possible games by a given bag ColorCount set.
fn get_possible_id_sum(lines: Lines, bag: &ColorCount) -> u32 {
    lines.fold(0, |mut acc, line| {
        let game = Game::from(line);

        if game.is_possible(bag) {
            acc += game.id;
        }

        acc
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_possible_id_sum() {
        let lines: Lines = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\r\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\r\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\r\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\r\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
            .lines();

        assert_eq!(
            get_possible_id_sum(
                lines,
                &ColorCount {
                    red: 12,
                    green: 13,
                    blue: 14,
                }
            ),
            8
        );
    }
}
