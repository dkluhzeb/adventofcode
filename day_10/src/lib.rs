use crate::utils::shoelace;
use std::{error::Error, fs::read_to_string, path::Path};

pub mod part_1;
pub mod part_2;
pub mod utils;

#[derive(Debug, PartialEq)]
pub struct Map {
    start: (usize, usize),
    grid: Vec<Vec<Field>>,
    cursor: Cursor,
    log: Option<Vec<(usize, usize)>>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Cursor {
    x: usize,
    y: usize,
    direction_x: Option<bool>,
    direction_y: Option<bool>,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Field {
    VerticalPipe,
    HorizontalPipe,
    BendNorthEast,
    BendNorthWest,
    BendSouthWest,
    BendSouthEast,
    Ground,
    Start,
}

impl Map {
    pub fn get_farthest_point(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        Ok(self.walk()? / 2)
    }

    pub fn enclosed_points(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        self.enable_log();
        self.walk()?;

        Ok(shoelace(&self.log.clone().expect("Missing log entries.")))
    }

    pub fn walk(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        if let Ok(steps) = self.walk_east() {
            return Ok(steps);
        }

        if let Ok(steps) = self.walk_south() {
            return Ok(steps);
        }

        if let Ok(steps) = self.walk_west() {
            return Ok(steps);
        }

        if let Ok(steps) = self.walk_north() {
            return Ok(steps);
        }

        Err("No way out.".into())
    }

    fn walk_east(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        self.cursor.x = self.cursor.x + 1;
        self.cursor.direction_x = Some(true);
        self.walk_loop()
    }

    fn walk_south(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        self.cursor.y = self.cursor.y + 1;
        self.cursor.direction_y = Some(true);
        self.walk_loop()
    }

    fn walk_west(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        self.cursor.x = self.cursor.x - 1;
        self.cursor.direction_x = Some(false);
        self.walk_loop()
    }

    fn walk_north(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        self.cursor.y = self.cursor.y - 1;
        self.cursor.direction_y = Some(false);
        self.walk_loop()
    }

    fn walk_loop(self: &mut Self) -> Result<usize, Box<dyn Error>> {
        let mut steps = 0;

        loop {
            steps = steps + 1;

            if let Some(log) = &mut self.log {
                log.push((self.cursor.x, self.cursor.y));
            }

            let current_field = self.current_field();

            if current_field == Field::Start {
                break;
            }

            self.cursor = current_field.next(self.cursor.clone())?;
        }

        Ok(steps)
    }

    fn current_field(self: &Self) -> Field {
        self.grid[self.cursor.y][self.cursor.x]
    }

    pub fn enable_log(self: &mut Self) {
        self.log = Some(Vec::from([(self.start.0, self.start.1)]));
    }
}

impl TryFrom<&Path> for Map {
    type Error = Box<dyn Error>;

    fn try_from(file_path: &Path) -> Result<Self, Box<dyn Error>> {
        let file_contents = read_to_string(file_path)?;
        let mut grid: Vec<Vec<Field>> = Vec::new();
        let mut start: (usize, usize) = (0, 0);

        for (y, line) in file_contents.lines().enumerate() {
            let mut row: Vec<Field> = Vec::new();

            for (x, char) in line.chars().enumerate() {
                if char == 'S' {
                    start = (x, y);
                }

                row.push(Field::try_from(char)?);
            }

            grid.push(row);
        }

        Ok(Map {
            start,
            grid,
            cursor: Cursor {
                x: start.0,
                y: start.1,
                direction_x: None,
                direction_y: None,
            },
            log: None,
        })
    }
}

impl Field {
    pub fn next(self: Self, cursor: Cursor) -> Result<Cursor, Box<dyn Error>> {
        let mut cursor: Cursor = Cursor {
            x: cursor.x,
            y: cursor.y,
            direction_x: cursor.direction_x,
            direction_y: cursor.direction_y,
        };
        match self {
            Field::VerticalPipe => match cursor.direction_y {
                Some(direction_y) => {
                    cursor.y = if direction_y {
                        cursor.y + 1
                    } else {
                        cursor.y - 1
                    };
                    cursor.direction_x = None;
                    Ok(cursor)
                }
                None => Err("Static cursor.".into()),
            },
            Field::HorizontalPipe => match cursor.direction_x {
                Some(direction_x) => {
                    cursor.x = if direction_x {
                        cursor.x + 1
                    } else {
                        cursor.x - 1
                    };
                    cursor.direction_y = None;
                    Ok(cursor)
                }
                None => Err("Static cursor.".into()),
            },
            //   #   Enter y or -x
            //   ##
            Field::BendNorthEast => match (cursor.direction_x, cursor.direction_y) {
                (Some(direction_x), Some(direction_y)) => {
                    if direction_x && !direction_y || !direction_x && direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    if !direction_x {
                        cursor.y = cursor.y - 1;
                        cursor.direction_x = None;
                        cursor.direction_y = Some(false);
                    }

                    if direction_y {
                        cursor.x = cursor.x + 1;
                        cursor.direction_x = Some(true);
                        cursor.direction_y = None;
                    }

                    Ok(cursor)
                }
                (Some(direction_x), None) => {
                    if direction_x {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.y = cursor.y - 1;
                    cursor.direction_x = None;
                    cursor.direction_y = Some(false);

                    Ok(cursor)
                }
                (None, Some(direction_y)) => {
                    if !direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.x = cursor.x + 1;
                    cursor.direction_x = Some(true);
                    cursor.direction_y = None;

                    Ok(cursor)
                }
                (None, None) => Err("Can not identify direction.".into()),
            },
            //    #  Enter y or x
            //   ##
            Field::BendNorthWest => match (cursor.direction_x, cursor.direction_y) {
                (Some(direction_x), Some(direction_y)) => {
                    if !direction_x && !direction_y || direction_x && direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    if direction_x {
                        cursor.y = cursor.y - 1;
                        cursor.direction_x = None;
                        cursor.direction_y = Some(false);
                    }

                    if direction_y {
                        cursor.x = cursor.x - 1;
                        cursor.direction_x = Some(false);
                        cursor.direction_y = None;
                    }

                    Ok(cursor)
                }
                (Some(direction_x), None) => {
                    if !direction_x {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.y = cursor.y - 1;
                    cursor.direction_x = None;
                    cursor.direction_y = Some(false);

                    Ok(cursor)
                }
                (None, Some(direction_y)) => {
                    if !direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.x = cursor.x - 1;
                    cursor.direction_x = Some(false);
                    cursor.direction_y = None;

                    Ok(cursor)
                }
                (None, None) => Err("Can not identify direction.".into()),
            },
            //   ##  Enter -y or x
            //    #
            Field::BendSouthWest => match (cursor.direction_x, cursor.direction_y) {
                (Some(direction_x), Some(direction_y)) => {
                    if !direction_x && direction_y || direction_x && !direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    if direction_x {
                        cursor.y = cursor.y + 1;
                        cursor.direction_x = None;
                        cursor.direction_y = Some(true);
                    }

                    if !direction_y {
                        cursor.x = cursor.x - 1;
                        cursor.direction_x = Some(false);
                        cursor.direction_y = None;
                    }

                    Ok(cursor)
                }
                (Some(direction_x), None) => {
                    if !direction_x {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.y = cursor.y + 1;
                    cursor.direction_x = None;
                    cursor.direction_y = Some(true);

                    Ok(cursor)
                }
                (None, Some(direction_y)) => {
                    if direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.x = cursor.x - 1;
                    cursor.direction_x = Some(false);
                    cursor.direction_y = None;

                    Ok(cursor)
                }
                (None, None) => Err("Can not identify direction.".into()),
            },
            //   ##  Enter -y or -x
            //   #
            Field::BendSouthEast => match (cursor.direction_x, cursor.direction_y) {
                (Some(direction_x), Some(direction_y)) => {
                    if direction_x && direction_y || !direction_x && !direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    if !direction_x {
                        cursor.y = cursor.y + 1;
                        cursor.direction_x = None;
                        cursor.direction_y = Some(true);
                    }

                    if !direction_y {
                        cursor.x = cursor.x + 1;
                        cursor.direction_x = Some(true);
                        cursor.direction_y = None;
                    }

                    Ok(cursor)
                }
                (Some(direction_x), None) => {
                    if direction_x {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.y = cursor.y + 1;
                    cursor.direction_x = None;
                    cursor.direction_y = Some(true);

                    Ok(cursor)
                }
                (None, Some(direction_y)) => {
                    if direction_y {
                        return Err("Can not identify direction.".into());
                    }

                    cursor.x = cursor.x + 1;
                    cursor.direction_x = Some(true);
                    cursor.direction_y = None;

                    Ok(cursor)
                }
                (None, None) => Err("Can not identify direction.".into()),
            },
            Field::Ground => Err("Running on ground.".into()),
            Field::Start => Ok(cursor),
        }
    }
}

impl TryFrom<char> for Field {
    type Error = Box<dyn Error>;

    fn try_from(c: char) -> Result<Self, Box<dyn Error>> {
        match c {
            '|' => Ok(Field::VerticalPipe),
            '-' => Ok(Field::HorizontalPipe),
            'L' => Ok(Field::BendNorthEast),
            'J' => Ok(Field::BendNorthWest),
            '7' => Ok(Field::BendSouthWest),
            'F' => Ok(Field::BendSouthEast),
            '.' => Ok(Field::Ground),
            'S' => Ok(Field::Start),
            _ => Err("Unknown Field.".into()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_map_from_path() {
        assert_eq!(
            Map::try_from(Path::new(
                format!("{}/fixtures/example", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_map()
        );

        assert_eq!(
            Map::try_from(Path::new(
                format!("{}/fixtures/example2", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap(),
            get_map_2()
        );
    }

    #[test]
    fn test_walk() {
        assert_eq!(get_map().walk().unwrap(), 8);
        assert_eq!(get_map_2().walk().unwrap(), 16);
    }

    #[test]
    fn test_enclosed_points() {
        assert_eq!(
            // TODO: decouple units!!!
            Map::try_from(Path::new(
                format!("{}/fixtures/example5", env!("CARGO_MANIFEST_DIR")).as_str()
            ))
            .unwrap()
            .enclosed_points()
            .unwrap(),
            8
        );
    }

    #[test]
    fn test_get_farthest_point() {
        assert_eq!(get_map().get_farthest_point().unwrap(), 4);
        assert_eq!(get_map_2().get_farthest_point().unwrap(), 8);
    }

    fn get_map() -> Map {
        Map {
            start: (1, 1),
            grid: Vec::from([
                Vec::from([
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Ground,
                    Field::Start,
                    Field::HorizontalPipe,
                    Field::BendSouthWest,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Ground,
                    Field::VerticalPipe,
                    Field::Ground,
                    Field::VerticalPipe,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Ground,
                    Field::BendNorthEast,
                    Field::HorizontalPipe,
                    Field::BendNorthWest,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                ]),
            ]),
            cursor: Cursor {
                x: 1,
                y: 1,
                direction_x: None,
                direction_y: None,
            },
            log: None,
        }
    }

    fn get_map_2() -> Map {
        Map {
            start: (0, 2),
            grid: Vec::from([
                Vec::from([
                    Field::Ground,
                    Field::Ground,
                    Field::BendSouthEast,
                    Field::BendSouthWest,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Ground,
                    Field::BendSouthEast,
                    Field::BendNorthWest,
                    Field::VerticalPipe,
                    Field::Ground,
                ]),
                Vec::from([
                    Field::Start,
                    Field::BendNorthWest,
                    Field::Ground,
                    Field::BendNorthEast,
                    Field::BendSouthWest,
                ]),
                Vec::from([
                    Field::VerticalPipe,
                    Field::BendSouthEast,
                    Field::HorizontalPipe,
                    Field::HorizontalPipe,
                    Field::BendNorthWest,
                ]),
                Vec::from([
                    Field::BendNorthEast,
                    Field::BendNorthWest,
                    Field::Ground,
                    Field::Ground,
                    Field::Ground,
                ]),
            ]),
            cursor: Cursor {
                x: 0,
                y: 2,
                direction_x: None,
                direction_y: None,
            },
            log: None,
        }
    }
}
