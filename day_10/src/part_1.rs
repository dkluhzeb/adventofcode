use crate::Map;
use std::{error::Error, path::Path};

/// The entry point for this puzzle part
/// Solution for https://adventofcode.com/2023/day/10
pub fn run(file_path: &Path) -> Result<usize, Box<dyn Error>> {
    Ok(Map::try_from(file_path)?.get_farthest_point()?)
}
