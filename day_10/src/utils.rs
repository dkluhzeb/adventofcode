pub fn shoelace(path_coords: &Vec<(usize, usize)>) -> usize {
    let mut a: usize = 0;
    let mut b: usize = 0;

    for (index, (x, y)) in path_coords.into_iter().enumerate() {
        if index == path_coords.len() - 1 {
            break;
        }

        a += x * path_coords[index + 1].1;
        b += y * path_coords[index + 1].0;
    }

    a.abs_diff(b) / 2 - path_coords.len() / 2 + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_shoelace() {
        assert_eq!(
            shoelace(&Vec::from([
                (1, 1),
                (2, 1),
                (3, 1),
                (3, 2),
                (3, 3),
                (2, 3),
                (1, 3),
                (1, 2),
                (1, 1),
            ])),
            1
        );
    }
}
